
#'
#' ---
#' title: an9elproject package
#' author: Angel Martinez-Perez
#' institute: Unit of Genomic of Complex Diseases
#' date: "Started: 2021-12-01"
#' description: Generator of data containers to store research projects
#' last update: "`r Sys.Date()`"
#' ---
#' 

#+ social, echo = FALSE, results = "asis"
an9el_social()
#' 
#+ toc, results = "asis", echo = FALSE
cat("[[_TOC_]]")

#+ load-libs, include = FALSE, echo = FALSE
suppressPackageStartupMessages({
    library(pacman)
    p_load( here, details, glue,
           knitr, knitcitations, summarytools,
           rmarkdown,
           an9elutils, an9elproject, an9elversions,
           DT, magrittr, plyr, data.table, tidyverse, dplyr,
           tsibble, withr, lumberjack, 
           GGally, testthat)
})
knitr::opts_chunk$set(fig.width = 12,
                      fig.height = 12,
                      fig.path = "source/images/",
                      echo = TRUE,
                      eval = TRUE,
                      warning = FALSE,
                      message = TRUE,
                      collapse = TRUE,
                      out.width = "100%")
opts_chunk$set(tidy = FALSE)
cacheoption <- TRUE
cleanbib()
cite_options(citation_format = "compatibility",
             hyperlink = "to.bib",
             cite.style = "authortitle",
             style = "markdown",
             max.names = 1,
             sorting = "ydnt")
## bib <- read.bibtex(file = "Doc.bib")

#'
#' # plots
#'
#' See this [web](https://mran.microsoft.com/snapshot/2020-04-29/web/packages/collapse/vignettes/collapse_and_plm.html) and [this web](https://sebkrantz.github.io/collapse/articles/collapse_and_plm.html#part-2-fast-exploration-of-panel-data) for more information.
#'
#'
naiv <-get_project('naiv')
data("naiv")

## descriptives:
collapse::qsu(long(naiv))

plot(long(naiv)$incre)
plot(collapse::psmat(long(naiv)$incre, transpose = TRUE), legend = TRUE)
plot(collapse::psmat(long(naiv), cols = 3:5), legend = TRUE)

## ‘psacf’ auto-correlation or covariance functions for panel series
k1 <- collapse::psacf(long(naiv), cols = 3:5, type = "correlation", gscale = TRUE)
k2 <- collapse::psacf(long(naiv), cols = 3:5, type = "covariance", gscale = TRUE)
k3 <- collapse::psacf(long(naiv), cols = 3:5, type = "partial", gscale = TRUE)

## ‘pspacf’ partial auto-correlation or covariance functions for panel series
collapse::pspacf(long(naiv), cols = 3:4, gscale = TRUE)

## ‘psccf’ cross-correlation or covariance functions for panel series
collapse::psccf(long(naiv)$incre, long(naiv)$decre)
k <- collapse::psccf(long(naiv)$incre, long(naiv)$equ)

kd <- long(naiv)$incre
k <- cbind(kd, fdiff(kd), D(kd), Dlog(kd), fcumsum(kd), flag(kd), L(kd), F(kd), fgrowth(kd), G(kd))

## scale within each subject
plot(c(long(naiv)$incre), fscale(long(naiv)$incre))
plot(c(fscale(long(naiv)$incre)), c(fwithin(long(naiv)$incre)))


## number of unique observations
fnunique(long(naiv)$incre)

# cumsum
fcumsum(long(naiv)$incre)
# growth
fgrowth(long(naiv)$incre)
# differences
fdiff(long(naiv)$incre)

## calcular el crecimiento en 30 días
fgrowth(long(naiv)$incre, shift = 30)

## calcular el crecimiento en 7 días, rellenando los Missing with the previous value of the cummulative sum
fgrowth(long(naiv)$incre, shift = 7, fill = TRUE)






## quasi differences for panel data
fdiff(long(naiv)$incre)
fdiff(long(naiv)$incre, n = 5)


fsubset(long(naiv), id %in% c("id_13", "id_40"), -decre)
fsubset(long(naiv), id %in% paste0("id_", 1:10), -c(id, id_long)) %>% qsu %>% as.data.frame

long(naiv) %>%
    fgroup_by(id) %>% fsummarise(sumincre = sum(incre), meanequ = mean(equ)) %>%
    roworder(meanequ) %>% head


long(naiv) %>%
    fgroup_by(id) %>% fsummarise(across(incre:equ, list(mean = fmean, min = fmin, max = fmax))) %>%
   head

########################################
Examples

## Let's start with some statistical programming
v <- iris$Sepal.Length
d <- num_vars(iris)    # Saving numeric variables
f <- iris$Species      # Factor

# Simple statistics
fmean(v)               # vector
fmean(qM(d))           # matrix (qM is a faster as.matrix)
fmean(d)               # data.frame

# Preserving data structure
fmean(qM(d), drop = FALSE)     # Still a matrix
fmean(d, drop = FALSE)         # Still a data.frame

# Weighted statistics, supported by most functions...
w <- abs(rnorm(fnrow(iris)))
fmean(d, w = w)

# Grouped statistics...
fmean(d, f)

# Groupwise-weighted statistics...
fmean(d, f, w)

# Simple Transformations...
head(fmode(d, TRA = "replace"))    # Replacing values with the mode
head(fmedian(d, TRA = "-"))        # Subtracting the median
head(fsum(d, TRA = "%"))           # Computing percentages
head(fsd(d, TRA = "/"))            # Dividing by the standard-deviation (scaling), etc...

# Weighted Transformations...
head(fnth(d, 0.75, w = w, TRA = "replace"))  # Replacing by the weighted 3rd quartile

# Grouped Transformations...
head(fvar(d, f, TRA = "replace"))  # Replacing values with the group variance
head(fsd(d, f, TRA = "/"))         # Grouped scaling
head(fmin(d, f, TRA = "-"))        # Setting the minimum value in each species to 0
head(fsum(d, f, TRA = "/"))        # Dividing by the sum (proportions)
head(fmedian(d, f, TRA = "-"))     # Groupwise de-median
head(ffirst(d, f, TRA = "%%"))     # Taking modulus of first group-value, etc. ...

# Grouped and weighted transformations...
head(fsd(d, f, w, "/"), 3)         # weighted scaling
head(fmedian(d, f, w, "-"), 3)     # subtracting the weighted group-median
head(fmode(d, f, w, "replace"), 3) # replace with weighted statistical mode

## Some more advanced transformations...
head(fbetween(d))                             # Averaging (faster t.: fmean(d, TRA = "replace"))
head(fwithin(d))                              # Centering (faster than: fmean(d, TRA = "-"))
head(fwithin(d, f, w))                        # Grouped and weighted (same as fmean(d, f, w, "-"))
head(fwithin(d, f, w, mean = 5))              # Setting a custom mean
head(fwithin(d, f, w, theta = 0.76))          # Quasi-centering i.e. d - theta*fbetween(d, f, w)
head(fwithin(d, f, w, mean = "overall.mean")) # Preserving the overall mean of the data
head(fscale(d))                               # Scaling and centering
head(fscale(d, mean = 5, sd = 3))             # Custom scaling and centering
head(fscale(d, mean = FALSE, sd = 3))         # Mean preserving scaling
head(fscale(d, f, w))                         # Grouped and weighted scaling and centering
head(fscale(d, f, w, mean = 5, sd = 3))       # Custom grouped and weighted scaling and centering
head(fscale(d, f, w, mean = FALSE,            # Preserving group means
            sd = "within.sd"))                # and setting group-sd to fsd(fwithin(d, f, w), w = w)
head(fscale(d, f, w, mean = "overall.mean",   # Full harmonization of group means and variances,
            sd = "within.sd"))                # while preserving the level and scale of the data.

head(get_vars(iris, 1:2))                      # Use get_vars for fast selecting, gv is shortcut
head(fhdbetween(gv(iris, 1:2), gv(iris, 3:5))) # Linear prediction with factors and covariates
head(fhdwithin(gv(iris, 1:2), gv(iris, 3:5)))  # Linear partialling out factors and covariates
ss(iris, 1:10, 1:2)                            # Similarly fsubset/ss for fast subsetting rows

# Simple Time-Computations..
head(flag(AirPassengers, -1:3))                # One lead and three lags
head(fdiff(EuStockMarkets,                     # Suitably lagged first and second differences
      c(1, frequency(EuStockMarkets)), diff = 1:2))
head(fdiff(EuStockMarkets, rho = 0.87))        # Quasi-differences (x_t - rho*x_t-1)
head(fdiff(EuStockMarkets, log = TRUE))        # Log-differences
head(fgrowth(EuStockMarkets))                  # Exact growth rates (percentage change)
head(fgrowth(EuStockMarkets, logdiff = TRUE))  # Log-difference growth rates (percentage change)
# Note that it is not necessary to use factors for grouping.
fmean(gv(mtcars, -c(2,8:9)), mtcars$cyl) # Can also use vector (internally converted using qF())
fmean(gv(mtcars, -c(2,8:9)),
      gv(mtcars, c(2,8:9)))              # or a list of vector (internally grouped using GRP())
g <- GRP(mtcars, ~ cyl + vs + am)        # It is also possible to create grouping objects
print(g)                                 # These are instructive to learn about the grouping,
plot(g)                                  # and are directly handed down to C++ code
fmean(gv(mtcars, -c(2,8:9)), g)          # This can speed up multiple computations over same groups
fsd(gv(mtcars, -c(2,8:9)), g)

# Factors can efficiently be created using qF()
f1 <- qF(mtcars$cyl)                     # Unlike GRP objects, factors are checked for NA's
f2 <- qF(mtcars$cyl, na.exclude = FALSE) # This can however be avoided through this option
class(f2)                                # Note the added class
 
library(microbenchmark)
microbenchmark(fmean(mtcars, f1), fmean(mtcars, f2)) # A minor difference, larger on larger data

with(mtcars, finteraction(cyl, vs, am))  # Efficient interactions of vectors and/or factors
finteraction(gv(mtcars, c(2,8:9)))       # .. or lists of vectors/factors

# Simple row- or column-wise computations on matrices or data frames with dapply()
dapply(mtcars, quantile)                 # column quantiles
dapply(mtcars, quantile, MARGIN = 1)     # Row-quantiles
  # dapply preserves the data structure of any matrices / data frames passed
  # Some fast matrix row/column functions are also provided by the matrixStats package
# Similarly, BY performs grouped comptations
BY(mtcars, f2, quantile)
BY(mtcars, f2, quantile, expand.wide = TRUE)
# For efficient (grouped) replacing and sweeping out computed statistics, use TRA()
sds <- fsd(mtcars)
head(TRA(mtcars, sds, "/"))     # Simple scaling (if sd's not needed, use fsd(mtcars, TRA = "/"))
 
microbenchmark(TRA(mtcars, sds, "/"), sweep(mtcars, 2, sds, "/")) # A remarkable performance gain..

sds <- fsd(mtcars, f2)
head(TRA(mtcars, sds, "/", f2)) # Groupd scaling (if sd's not needed: fsd(mtcars, f2, TRA = "/"))

# All functions above perserve the structure of matrices / data frames
# If conversions are required, use these efficient functions:
mtcarsM <- qM(mtcars)                      # Matrix from data.frame
head(qDF(mtcarsM))                         # data.frame from matrix columns
head(mrtl(mtcarsM, TRUE, "data.frame"))    # data.frame from matrix rows, etc..
head(qDT(mtcarsM, "cars"))                 # Saving row.names when converting matrix to data.table
head(qDT(mtcars, "cars"))                  # Same use a data.frame
 
## Now let's get some real data and see how we can use this power for data manipulation
library(magrittr)
head(wlddev) # World Bank World Development Data: 216 countries, 61 years, 5 series (columns 9-13)

# Starting with some discriptive tools...
namlab(wlddev, class = TRUE)           # Show variable names, labels and classes
fnobs(wlddev)                          # Observation count
pwnobs(wlddev)                         # Pairwise observation count
head(fnobs(wlddev, wlddev$country))    # Grouped observation count
fndistinct(wlddev)                     # Distinct values
descr(wlddev)                          # Describe data
varying(wlddev, ~ country)             # Show which variables vary within countries
qsu(wlddev, pid = ~ country,           # Panel-summarize columns 9 though 12 of this data
    cols = 9:12, vlabels = TRUE)       # (between and within countries)
qsu(wlddev, ~ region, ~ country,       # Do all of that by region and also compute higher moments
    cols = 9:12, higher = TRUE)        # -> returns a 4D array
qsu(wlddev, ~ region, ~ country, cols = 9:12,
    higher = TRUE, array = FALSE) %>%                           # Return as a list of matrices..
unlist2d(c("Variable","Trans"), row.names = "Region") %>% head  # and turn into a tidy data.frame
pwcor(num_vars(wlddev), P = TRUE)                           # Pairwise correlations with p-value
pwcor(fmean(num_vars(wlddev), wlddev$country), P = TRUE)    # Correlating country means
pwcor(fwithin(num_vars(wlddev), wlddev$country), P = TRUE)  # Within-country correlations
psacf(wlddev, ~country, ~year, cols = 9:12)                 # Panel-data Autocorrelation function
pspacf(wlddev, ~country, ~year, cols = 9:12)                # Partial panel-autocorrelations
psmat(wlddev, ~iso3c, ~year, cols = 9:12) %>% plot          # Convert panel to 3D array and plot

## collapse offers a few very efficent functions for data manipulation:
# Fast selecting and replacing columns
series <- get_vars(wlddev, 9:12)     # Same as wlddev[9:12] but 2x faster
series <- fselect(wlddev, PCGDP:ODA) # Same thing: > 100x faster than dplyr::select
get_vars(wlddev, 9:12) <- series     # Replace, 8x faster wlddev[9:12] <- series + replaces names
fselect(wlddev, PCGDP:ODA) <- series # Same thing

# Fast subsetting
head(fsubset(wlddev, country == "Ireland", -country, -iso3c))
head(fsubset(wlddev, country == "Ireland" & year > 1990, year, PCGDP:ODA))
ss(wlddev, 1:10, 1:10) # This is an order of magnitude faster than wlddev[1:10, 1:10]

# Fast transforming
head(ftransform(wlddev, ODA_GDP = ODA / PCGDP, ODA_LIFEEX = sqrt(ODA) / LIFEEX))
settransform(wlddev, ODA_GDP = ODA / PCGDP, ODA_LIFEEX = sqrt(ODA) / LIFEEX) # by reference
head(ftransform(wlddev, PCGDP = NULL, ODA = NULL, GINI_sum = fsum(GINI)))
head(ftransformv(wlddev, 9:12, log))           # Can also transform with lists of columns
head(ftransformv(wlddev, 9:12, fscale, apply = FALSE)) # apply = FALSE invokes fscale.data.frame
settransformv(wlddev, 9:12, fscale, apply = FALSE)     # Changing the data by reference
ftransform(wlddev) <- fscale(gv(wlddev, 9:12))         # Same thing (using replacement method)

wlddev %<>% ftransformv(9:12, fscale, apply = FALSE) # Same thing, using magrittr
wlddev %>% ftransform(gv(., 9:12) %>%              # With compound pipes: Scaling and lagging
                        fscale %>% flag(0:2, iso3c, year)) %>% head

# Fast reordering
head(roworder(wlddev, -country, year))
head(colorder(wlddev, country, year))

# Fast renaming
head(frename(wlddev, country = Ctry, year = Yr))
setrename(wlddev, country = Ctry, year = Yr)     # By reference
head(frename(wlddev, tolower, cols = 9:12))

# Fast grouping
fgroup_by(wlddev, Ctry, decade) %>% fgroup_vars %>% head # fgroup_by is faster than dplyr::group_by

rm(wlddev)                                       # .. but only works with collapse functions

## Now lets start putting things together
wlddev %>% fsubset(year > 1990, region, income, PCGDP:ODA) %>%
  fgroup_by(region, income) %>% fmean            # Fast aggregation using the mean

# Same thing using dplyr manipulation verbs
library(dplyr)
wlddev %>% filter(year > 1990) %>% select(region, income, PCGDP:ODA) %>%
  group_by(region,income) %>% fmean       # This is already a lot faster than summarize(across(everything(), mean, na.rm = TRUE))

wlddev %>% fsubset(year > 1990, region, income, PCGDP:POP) %>%
  fgroup_by(region, income) %>% fmean(POP)     # Weighted group means

wlddev %>% fsubset(year > 1990, region, income, PCGDP:POP) %>%
  fgroup_by(region, income) %>% fsd(POP)       # Weighted group standard deviations

wlddev %>% na_omit(cols = "POP") %>% fgroup_by(region, income) %>%
  fselect(PCGDP:POP) %>% fnth(0.75, POP)       # Weighted group third quartile

wlddev %>% fgroup_by(country) %>% fselect(PCGDP:ODA) %>%
fwithin %>% head                               # Within transformation
wlddev %>% fgroup_by(country) %>% fselect(PCGDP:ODA) %>%
fmedian(TRA = "-") %>% head                    # Grouped centering using the median
# Replacing data points by the weighted first quartile:
wlddev %>% na_omit(cols = "POP") %>% fgroup_by(country) %>%
  fselect(country, year, PCGDP:POP) %>%
  ftransform(fselect(., -country, -year) %>%
  fnth(0.25, POP, "replace_fill")) %>% head

wlddev %>% fgroup_by(country) %>% fselect(PCGDP:ODA) %>% fscale %>% head    # Standardizing
wlddev %>% fgroup_by(country) %>% fselect(PCGDP:POP) %>%
   fscale(POP) %>% head  # Weigted..

wlddev %>% fselect(country, year, PCGDP:ODA) %>%  # Adding 1 lead and 2 lags of each variable
  fgroup_by(country) %>% flag(-1:2, year) %>% head
wlddev %>% fselect(country, year, PCGDP:ODA) %>%  # Adding 1 lead and 10-year growth rates
  fgroup_by(country) %>% fgrowth(c(0:1,10), 1, year) %>% head

# etc...

# Aggregation with multiple functions
wlddev %>% fsubset(year > 1990, region, income, PCGDP:ODA) %>%
  fgroup_by(region, income) %>% {
    add_vars(fgroup_vars(., "unique"),
             fmedian(., keep.group_vars = FALSE) %>% add_stub("median_"),
             fmean(., keep.group_vars = FALSE) %>% add_stub("mean_"),
             fsd(., keep.group_vars = FALSE) %>% add_stub("sd_"))
  } %>% head

# Transformation with multiple functions
wlddev %>% fselect(country, year, PCGDP:ODA) %>%
  fgroup_by(country) %>% {
    add_vars(fdiff(., c(1,10), 1, year) %>% flag(0:2, year),  # Sequence of lagged differences
             ftransform(., fselect(., PCGDP:ODA) %>% fwithin %>% add_stub("W.")) %>%
               flag(0:2, year, keep.ids = FALSE))             # Sequence of lagged demeaned vars
  } %>% head

# With ftransform, can also easily do one or more grouped mutations on the fly..
settransform(wlddev, median_ODA = fmedian(ODA, list(region, income), TRA = "replace_fill"))

settransform(wlddev, sd_ODA = fsd(ODA, list(region, income), TRA = "replace_fill"),
                     mean_GDP = fmean(PCGDP, country, TRA = "replace_fill"))

wlddev %<>% ftransform(fmedian(list(median_ODA = ODA, median_GDP = PCGDP),
                               list(region, income), TRA = "replace_fill"))

# On a groped data frame it is also possible to grouped transform certain columns
# but perform aggregate operatins on others:
wlddev %>% fgroup_by(region, income) %>%
    ftransform(gmedian_GDP = fmedian(PCGDP, GRP(.), TRA = "replace"),
               omedian_GDP = fmedian(PCGDP, TRA = "replace"),  # "replace" preserves NA's
               omedian_GDP_fill = fmedian(PCGDP)) %>% tail

rm(wlddev)

## For multi-type data aggregation, the function collap offers ease and flexibility
# Aggregate this data by country and decade: Numeric columns with mean, categorical with mode
head(collap(wlddev, ~ country + decade, fmean, fmode))

# taking weighted mean and weighted mode:
head(collap(wlddev, ~ country + decade, fmean, fmode, w = ~ POP, wFUN = fsum))

# Multi-function aggregation of certain columns
head(collap(wlddev, ~ country + decade,
            list(fmean, fmedian, fsd),
            list(ffirst, flast), cols = c(3,9:12)))

# Customized Aggregation: Assign columns to functions
head(collap(wlddev, ~ country + decade,
            custom = list(fmean = 9:10, fsd = 9:12, flast = 3, ffirst = 6:8)))

# For grouped data frames use collapg
wlddev %>% fsubset(year > 1990, country, region, income, PCGDP:ODA) %>%
  fgroup_by(country) %>% collapg(fmean, ffirst) %>%
  ftransform(AMGDP = PCGDP > fmedian(PCGDP, list(region, income), TRA = "replace_fill"),
             AMODA = ODA > fmedian(ODA, income, TRA = "replace_fill")) %>% head

## Additional flexibility for data transformation tasks is offerend by tidy transformation operators
# Within-transformation (centering on overall mean)
head(W(wlddev, ~ country, cols = 9:12, mean = "overall.mean"))
# Partialling out country and year fixed effects
head(HDW(wlddev, PCGDP + LIFEEX ~ qF(country) + qF(year)))
# Same, adding ODA as continuous regressor
head(HDW(wlddev, PCGDP + LIFEEX ~ qF(country) + qF(year) + ODA))
# Standardizing (scaling and centering) by country
head(STD(wlddev, ~ country, cols = 9:12))
# Computing 1 lead and 3 lags of the 4 series
head(L(wlddev, -1:3, ~ country, ~year, cols = 9:12))
# Computing the 1- and 10-year first differences
head(D(wlddev, c(1,10), 1, ~ country, ~year, cols = 9:12))
head(D(wlddev, c(1,10), 1:2, ~ country, ~year, cols = 9:12))     # ..first and second differences
# Computing the 1- and 10-year growth rates
head(G(wlddev, c(1,10), 1, ~ country, ~year, cols = 9:12))
# Adding growth rate variables to dataset
add_vars(wlddev) <- G(wlddev, c(1, 10), 1, ~ country, ~year, cols = 9:12, keep.ids = FALSE)
get_vars(wlddev, "G1.", regex = TRUE) <- NULL # Deleting again

# These operators can conveniently be used in regression formulas:
# Using a Mundlak (1978) procedure to estimate the effect of OECD on LIFEEX, controlling for PCGDP
lm(LIFEEX ~ log(PCGDP) + OECD + B(log(PCGDP), country),
   wlddev %>% fselect(country, OECD, PCGDP, LIFEEX) %>% na_omit)

# Adding 10-year lagged life-expectancy to allow for some convergence effects (dynamic panel model)
lm(LIFEEX ~ L(LIFEEX, 10, country) + log(PCGDP) + OECD + B(log(PCGDP), country),
   wlddev %>% fselect(country, OECD, PCGDP, LIFEEX) %>% na_omit)

# Tranformation functions and operators also support indexed data classes:
wldi <- findex_by(wlddev, country, year)
head(W(wldi$PCGDP))                      # Country-demeaning
head(W(wldi, cols = 9:12))
head(W(wldi$PCGDP, effect = 2))          # Time-demeaning
head(W(wldi, effect = 2, cols = 9:12))
head(HDW(wldi$PCGDP))                    # Country- and time-demeaning
head(HDW(wldi, cols = 9:12))
head(STD(wldi$PCGDP))                    # Standardizing by country
head(STD(wldi, cols = 9:12))
head(L(wldi$PCGDP, -1:3))                # Panel-lags
head(L(wldi, -1:3, 9:12))
head(G(wldi$PCGDP))                      # Panel-Growth rates
head(G(wldi, 1, 1, 9:12))

lm(Dlog(PCGDP) ~ L(Dlog(LIFEEX), 0:3), wldi)   # Panel data regression
rm(wldi)

# Remove all objects used in this example section
rm(v, d, w, f, f1, f2, g, mtcarsM, sds, series, wlddev)



#######################################

#' # old
#' 
#' If you change the class of the object from \code{pdata.frame} to \code{tstibble} you
#' can benefic from their features.

#' If your project have longitudinal data it is stored in the **longitudinal** slot.   
#' You can use there all the power of the [tsibble](https://tsibble.tidyverts.org),
#' [feasts](https://feasts.tidyverts.org) and the [fable](https://fable.tidyverts.org) packages.
#' 
#+ cache = cacheoption, eval = TRUE, include = TRUE, echo = TRUE

example$longitudinal %>% group_by_key() %>%
    index_by(Year_Month = ~ yearmonth(.)) %>%
    summarise(
        mean_incre = mean(incre, na.rm = TRUE),
        sum_equ = sum(equ, na.rm = TRUE)) %>% head(7)
example$longitudinal %>% group_by_key(sex) %>%
    index_by(Quarterly = ~ yearquarter(.)) %>%
    summarise(
        mean_incre = mean(incre, na.rm = TRUE),
        sum_equ = sum(equ, na.rm = TRUE)) %>% head(7)
example$longitudinal %>% group_by() %>%
    summarise(
        mean_incre = mean(incre, na.rm = TRUE),
        sum_equ = sum(equ, na.rm = TRUE)) %>% head(7)
#'
#' Join the longitudinal data with some regular features
#' 
#+ cache = cacheoption, eval = TRUE, include = TRUE, echo = TRUE
kk <- example$longitudinal %>%
    dplyr::left_join(example[, c("id", "sex")],
                     by = "id")
kk %>% feasts::gg_season(incre)

## let's plot only 10 subjects separated by sex
#+ eval = FALSE
withr::with_seed(seed = 1, kk <- kk %>% brolgar::sample_n_keys(size = 10))
kk %>% ggplot(aes(x = id_long,
                  y = incre,
                  group_by = id,
                  color = id)) +
    scale_x_date(date_labels = c("%b %Y")) +
    theme(axis.text.x = element_text(angle = 30, hjust = 1)) +
    geom_line() +
    facet_wrap(~sex)

#'
#' ---
#'

#' we can know the summary statistics for each individual with:
#' 
#+ results="asis",  eval = FALSE
kk %>% fabletools::features(incre, brolgar::feat_five_num) %>%
  head(3) %>% data.frame %>% dplyr::mutate(across(where(is.numeric), round, digits = 2))
kk %>% fabletools::features(incre, brolgar::feat_brolgar) %>%
  head(3) %>% data.frame %>% dplyr::mutate(across(where(is.numeric), round, digits = 1))
kk %>% fabletools::features(incre, brolgar::feat_diff_summary) %>%
  head(3) %>% data.frame %>% dplyr::mutate(across(where(is.numeric), round, digits = 1))
kk %>% fabletools::features(incre, feasts::feat_acf) %>% head(3) %>%
    data.frame %>% dplyr::mutate(across(where(is.numeric), round, digits = 1))
n_obs(kk)## number of observations
n_keys(kk)## number of subjects
kk %>%## number of observation per subject
    features(incre, n_obs) %>%
    ggplot(aes(x = n_obs)) +
    geom_bar()
kk %>% key_slope( incre ~ id_long) %>% head(3)


#'
#' Which subjects have gaps (time points with missing values)?
#+ results="asis", echo = FALSE,  eval = FALSE
kk %>% dplyr::mutate(id_long = yearmonth(id_long)) %>%
    has_gaps() %>% head(3)

#'
#' Let's group the observations by months and visualize which months have gaps
#+ results="asis", echo = FALSE,  eval = FALSE
kk %>% dplyr::mutate(id_long = yearmonth(id_long)) %>%
    scan_gaps() %>%  head(4)
#'
#' Let's order the number of gaps per subject and per month
#+ results="asis", echo = FALSE,  eval = FALSE
kk %>% dplyr::mutate(id_long = yearmonth(id_long)) %>%
    count_gaps() %>% arrange(desc(.n)) %>% head(4)

#################################


## kk %>% feasts::gg_subseries(decre) ## repare name id
## kk %>% gg_lag(decre)
## k <- longitudinal %>% dplyr::mutate(across(matches('fech')), as.Date, format = "%m/%d/%y"))
## insert the names in the trait object first previously
## https://cran.rstudio.com/web/packages/tsibble/vignettes/intro-tsibble.html


#' 
#' # SESSION INFO
#' 
#+ cache = FALSE, echo = FALSE
an9elutils::sessionInfo()

