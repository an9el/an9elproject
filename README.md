
---
title: an9elproject package
author: Angel Martinez-Perez
institute: Unit of Genomic of Complex Diseases
date: "Started: 2021-12-01"
description: Generator of data containers to store research projects
last update: "2025-02-06"
---


an9elproject <a href='https://gitlab.com/an9el/an9elproject'><img src='inst/figures/logo.png' align='right' height='138' /></a><!-- social: start -->
[![](/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/orcid.png)](https://orcid.org/0000-0002-5934-1454 'ORCID')[![](/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/linkedin.png)](https://es.linkedin.com/in/angel-martinez-perez-28116a9 'linkedin')[![](/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/scholar.png)](http://scholar.google.es/citations?user=IleVNpwAAAAJ 'google scholar')[![](/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/rg.png)](https://www.researchgate.net/profile/Angel_Martinez-Perez/publications/ 'researchgate')[![](/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/pubmed.png)](https://www.ncbi.nlm.nih.gov/myncbi/1liNzajtgcWkjb/bibliography/public/ 'pubmed')[![](/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/twitter.png)](https://twitter.com/an9el_mp 'twitter')[<img src=/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/IR.png width='60' height='24'/>](http://www.recercasantpau.cat/es/grupo/genomica-y-bioinformatica-de-enfermedades-de-base-genetica-compleja 'IIB Sant Pau')[<img src=/home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/an9elutils/figures/wos.png width='24' height='24'/>](https://www.webofscience.com/wos/author/record/2108096 'WoS')
<!-- social: end -->



[[_TOC_]]



# Installation



``` r
devtools::install_gitlab("an9el/an9elproject")
pak::pkg_install("gitlab::an9el/an9elproject")
```


# Motivation

This package is born out of frustration every time we start a research project. Some of the
vices that I repeatedly run into, and that this package tries to mitigate, are as follows:

* Lack of proper definition of the project (name, description, goals, leader, start and ending date, ...)
* Lack of scope of the project (samples, variables, location, ...)
* Lack of the proper definition of the variables to collect (no definition at all, bad names, inconsistencies, duplicates, multiple versions,
lack of data validation and version control, lack of units, lack creator, lack of comments...).
[See this web](https://bioportal.bioontology.org/ontologies/EFO?p=classes) for a proper way to name variables.
* Lack of version control, especially when some project are managed for multiple people, some of them in multiple machines.
* Lack of tools for search information within the project.

The main improvement of this way of working will be a unified interface for the treatment of projects. In this way,
the source code can be reused from one to another, changing only the project name.

In a future, for example, I can create methods easily for combine projects.

## Main characteristics

This package creates the skeleton of a generic research project, in an object of class `S3`.
This object has associated methods, for the introduction and elimination of data, automatic testing (in redundant
data, bad names, missing information, ...).

This package is complemented with the package [an9elversions](http://gitlab.com/an9el/an9elversions) which manages
a little database with the individual projects. It store the following data:

* The location of every type of  data in every enabled computer
* The history of changes (when is made, for whom, and for what reason)



# Create an an9elproject object to store a project

A research project consists on a minimum three types of data (data, data definition and
categories). It also have some miscelanea data to storage main characteristic of the project
and some longitudinal data:

1. A data.frame with the values of each variable gathered for each subject. (Dimension n subject x m variables)
1. A data.frame with all the characteristics attributable to each variable. (Dimension m variables x p characteristics)
1. A list of unknown length with groupings, of both individuals and variables that have biological or procedural meaning.
1. Miscelanea list to store main characteristic of the project
1. A slot to store longitudinal data if this exists.

For example:

<img src="source/images/naive_data-1.png" width="100%" />
<div class="kable-table">

|id   |sex |       tr1| tr2| tr.3|     tr 4|     tr+5|
|:----|:---|---------:|---:|----:|--------:|--------:|
|id_1 |F   |  9.730043|   1|    1|   0.0000|       NA|
|id_2 |M   | 10.073756|   2|    2| 100.5887| 49.99814|
|id_3 |F   | 10.341624|   3|    3| 100.1942| 50.57069|

</div><div class="kable-table">

|id    |id_long    |    incre|     decre|        equ|
|:-----|:----------|--------:|---------:|----------:|
|id_46 |2020-05-10 | 21.86254| 11.518266|  0.3824876|
|id_58 |2021-01-06 | 21.91702|  6.696154| -1.2995526|
|id_6  |2021-02-07 | 27.85410|  3.006428|  0.1806304|

</div><div class="kable-table">

|trait   |definition              |units      |contact          |email                      |
|:-------|:-----------------------|:----------|:----------------|:--------------------------|
|id      |sample id               |character  |Dr. Isaac Newton |the_real_newton@gosgle.com |
|sex     |sex                     |dichotomic |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr1     |trait number1           |kg         |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr2     |trait number2           |NA         |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr.3    |trait number3           |NA         |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr 4    |trait number4           |m          |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr+5    |trait number5           |cm         |Dr. Isaac Newton |the_real_newton@gosgle.com |
|id_long |temporal id used as key |date       |Dr. follow up    |the_real_newton@gosgle.com |
|incre   |longitudinal trait1     |cm         |Dr. follow up    |the_real_newton@gosgle.com |
|decre   |longitudinal trait2     |m          |Dr. follow up    |the_real_newton@gosgle.com |
|equ     |longitudinal trait3     |NA         |Dr. follow up    |the_real_newton@gosgle.com |

</div>


Some features names are mandatory:

* **id** for the *Normal features* data.frame
* **trait** for the *Feature characterization* data.frame
* **definition** for the *Feature characterization* data.frame
* **id_long** in the "Temporal features" data frame in case of exists

Be aware that all the features names must exist in the "Feature characterization" data.frame,
The normal and the temporal features.

With those data.frames, lets create our *an9elproject* object



``` r
example <- an9elproject::an9elproject(name = "Naive",
                        dt = dt_project,
                        traits = dt_traits,
                        categories = dt_categories)
```


It complains about the name of the traits. It gives you the list of bad names and a proposal of
better names.

keeping this names, as indicated, is not recommended, future problems are guaranteed. The best
time to name traits properly is in the very conception of the project (the project prototype itself).
If you are lucky enough to get traits with questionable names, the best thing to do is it change ASAP.

... but we love risk, so lets force to keep them anyway.



```r
example <- an9elproject::an9elproject(name = "Naive2",
                        dt = dt_project,
                        traits = dt_traits,
                        categories = dt_categories,
                        forceNames = TRUE,
                        longitudinal = dt_long)
```

```r
example <- an9elproject:::naiv
```


Here we see a lot of information.

To track all the versions of different project we use the `an9elversions` package


```r
remotes::install_gitlab('an9el/an9elversions')
```


The creation of the `an9elproject' object create a temporal file called
`to_an9elversions_xxxxxxxxxxx.csv`, this file must be used to update the package `an9elversions`

Withing the `gitlab` folder of the `an9elversions` package use the function:

> `an9elversions::new_track('path_to_file/to_an9elversions_xxxxxxxxxxx.csv')`

and then the usual, stage, commit and pull.

Now if you update your `an9elversions` package the new version is available.

Also you have to put your both the old version and the new version in the correct folder project

Please modify the `an9elversions::get_project` function to know where to put the project files in your machine(nodename) or if do not exist previously, modify it to become available after update the package.

# an9elproject category

Lets see how looks our project


```r
example
## 
## ── naiv project with length = 7 ──
## 
## ℹ  (version 0.0.8000, last update: 2022-09-20)
## ✔ 100 subjects
## ✔ 11 traits
## ✔ In 4 categories
## ℹ Longitudinal data:
## Unbalanced Panel: n = 100, T = 2-17, N = 850
## ✖ naiv project problem with duplicates
## ✖ naiv project problem with bad_names
names(example)
## [1] "data"         "traits"       "categories"   "version"      "longitudinal"
## [6] "warnings"     "misc"
example$categories
## $factors
## [1] "sex"
## 
## $indices
## [1] "tr1"  "tr.3"
## 
## $project1
## [1] "id"   "sex"  "tr+5" "tr2" 
## 
## $project_long
## [1] "incre" "decre" "equ"
```
<div class="kable-table">

|id   |sex |       tr1| tr2| tr.3|     tr 4|     tr+5|
|:----|:---|---------:|---:|----:|--------:|--------:|
|id_1 |F   |  9.730043|   1|    1|   0.0000|       NA|
|id_2 |M   | 10.073756|   2|    2| 100.5887| 49.99814|
|id_3 |F   | 10.341624|   3|    3| 100.1942| 50.57069|

</div><div class="kable-table">

|id   |id_long    |      incre|    decre|      equ|
|:----|:----------|----------:|--------:|--------:|
|id_1 |2019-05-10 |  9.4398057| 26.88160| 1.628012|
|id_1 |2019-09-02 | -0.2095941| 35.18926| 0.393461|
|id_1 |2020-02-07 | 22.7962998| 20.28881| 1.598896|

</div><div class="kable-table">

|trait |definition    |units      |contact          |email                      |
|:-----|:-------------|:----------|:----------------|:--------------------------|
|id    |sample id     |character  |Dr. Isaac Newton |the_real_newton@gosgle.com |
|sex   |sex           |dichotomic |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr1   |trait number1 |kg         |Dr. Isaac Newton |the_real_newton@gosgle.com |

</div>

Lets examine the project with some functions


```r
dim(example)
## [1] 100   7
colnames(example)
## [1] "id"   "sex"  "tr1"  "tr2"  "tr.3" "tr 4" "tr+5"
head(row.names(example))
## [1] "id_1" "id_2" "id_3" "id_4" "id_5" "id_6"
```

```r
an9elproject::describe(example)
```

<div class="kable-table">

|Variable |Class     |   N| Ndist|      Mean|         SD|      Min|       Max|       Skew|      Kurt|        5%|       50%|       95%|
|:--------|:---------|---:|-----:|---------:|----------:|--------:|---------:|----------:|---------:|---------:|---------:|---------:|
|id       |character | 100|   100|        NA|         NA|       NA|        NA|         NA|        NA|        NA|        NA|        NA|
|sex      |factor    | 100|     2|        NA|         NA|       NA|        NA|         NA|        NA|        NA|        NA|        NA|
|tr1      |numeric   | 100|   100|  9.998296|  0.8926458|  7.88201|  12.41423|  0.0536673|  3.327514|  8.394027|  10.13513|  11.40484|
|tr2      |integer   | 100|   100| 50.500000| 29.0114920|  1.00000| 100.00000|  0.0000000|  1.799760|  5.950000|  50.50000|  95.05000|
|tr.3     |integer   | 100|   100| 50.500000| 29.0114920|  1.00000| 100.00000|  0.0000000|  1.799760|  5.950000|  50.50000|  95.05000|
|tr 4     |numeric   | 100|   100| 99.045378| 10.0540814|  0.00000| 102.61903| -9.7019140| 96.094993| 98.128886| 100.04641| 101.61763|
|tr+5     |numeric   |  98|    98| 49.949130|  0.9186450| 47.73156|  52.08262| -0.0263655|  2.691412| 48.567814|  50.06692|  51.48108|

</div>


# an9elproject inquiries, How to..

## Summary of methods for `an9elproject` class



``` r
man(example)
## 
## ── Summary ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────
## 1. Methods:
## ℹ Get the source code with `getAnywhere()`
##   • man() This help
##   • print Print summary of the object
##   • [] Direct access to data slot, `long()` for longitudinal slot
##   • dim `dim()` function in data slot
##   • rownames Access to data rownames
##   • colnames Access to data colnames
##   • dimnames Access to both row and colnames
##   • lindex Access to longitudinal index, usually id and id_long
##   • head Head of data slot
##   • kinship_name Return the kinship of two pairs of subject
## 2. Inserting/Updating/Deleting:
## ℹ Used to modifying the object (imply version update)
##   • addTraits/deleteTraits/deleteSubjects Add/delete features to traits slot
##   • update_trait Update features characterization in traits slot
##   • update_values Update feature values in data slot
##   • miscelanea Add miscelanea in the misc slot
## 3. Using the object:
## ℹ Mainly to retrieve information
##   • get_project function to use a project
##   • [], dim, rownames, colnames, dimnames and head As usual
##   • findTraits search a pattern in features names or definition
##   • long Access to the longitudinal slot
##   • get_versions to retrieve all the versions of a project
## 4. Utilities:
##   • describe Extract some descriptive of the selected features
##   • findDuplicates find duplicated rows in a `an9elproject` or a `data.frame` object
##   • log_level change verbosity of the whole project, min = 1, max = 6
##   • an9el2plink Create a `plink-format` file with some features
##   • check_names Validate feature names
##   • check_project Validate the project, duplicates, names and validation rules
##   • validate Validate all the rules in the validation slot
##   • check_last_version_online check if this is the lastest version
## 5. Plots:
## ℹ Need to load the ggan9el package
##   • plot a `points` or `missing` plot in a an9elproject object
##   • plot_pedigree plot the tree of the pedigree
##   • summary_versions plot the history of versions of all projects
```


## Search for features:

We can search for normal or temporal features. `findTraits` search for a pattern in the
`trait` and `definition` column of the `traits` slot. The search can be case sensitive or not 



```r
findTraits(obj = example, pattern = 'SEX', ignore_case = FALSE)
```

<div class="kable-table">

|trait |definition |units |contact |email |
|:-----|:----------|:-----|:-------|:-----|

</div>

```r
findTraits(obj = example, pattern = 'T Numb')
```

<div class="kable-table">

|trait |definition    |units |contact          |email                      |
|:-----|:-------------|:-----|:----------------|:--------------------------|
|tr1   |trait number1 |kg    |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr2   |trait number2 |NA    |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr.3  |trait number3 |NA    |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr 4  |trait number4 |m     |Dr. Isaac Newton |the_real_newton@gosgle.com |
|tr+5  |trait number5 |cm    |Dr. Isaac Newton |the_real_newton@gosgle.com |

</div>

```r
findTraits(obj = example, pattern = 'r.3')
```

<div class="kable-table">

|trait |definition    |units |contact          |email                      |
|:-----|:-------------|:-----|:----------------|:--------------------------|
|tr.3  |trait number3 |NA    |Dr. Isaac Newton |the_real_newton@gosgle.com |

</div>

```r
findTraits(obj = example, pattern = ' trait')
```

<div class="kable-table">

|trait |definition          |units |contact       |email                      |
|:-----|:-------------------|:-----|:-------------|:--------------------------|
|incre |longitudinal trait1 |cm    |Dr. follow up |the_real_newton@gosgle.com |
|decre |longitudinal trait2 |m     |Dr. follow up |the_real_newton@gosgle.com |
|equ   |longitudinal trait3 |NA    |Dr. follow up |the_real_newton@gosgle.com |

</div>



## Find duplicates features.


The rownames of the output are the duplicated variables, and columns indicates if we find
the duplicated variable in the normal or in the reverse order.



```
##      duplicate duplicate_rev
## tr.3      TRUE         FALSE
## tr2      FALSE          TRUE
```


# Modifying the object

When we modify an `an9elpackage` object, the following things happen (`log_level(6)`):

* check if we are modifying from the last version online
* check if we are trying to update the longitudinal part or the feature part of the data
* check if the actual traits exists
* check if the subjects exists
* log all the changes
* Ask for the new version and a description
* check for the integrity of the an9elobject
* check if there are duplicates in the project
* check the names of the features
* check for duplicates samples
* if the validation slot exists, validate the data
* create the file with the new object
* create the file to update the version with `an9elversions` package
* create a excel file with three sheets with the changes made.

## deleting traits

For modify the object, the object must be the last version of the project, if not there will be an error. For that, the project to be modified have to be registered with the `an9elversions` package.

If you do not specify any traits to delete, it will delete all the traits with all missing values.



```r
example <- an9elproject:::naiv
example2 <- deleteTraits(example, "tr.3", new_version = c("0.0.8010", "remove tr.3"))
## ! Deleting tr.3 feature.
## ! Traits will also be deleted from indices category
## ℹ Actual version: 0.0.8000
## ℹ No validation rules
## 
## ── Tracking version with `an9elversions` package ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
## 
## ── New version stored in the file:
## • ./to_an9elversions_150d0a5c893455.csv
## ℹ Withing the `gitlab` repository of the `an9elversions` package use in R:
## an9elversions::new_track('path_to_file/to_an9elversions_150d0a5c893455.csv')
## ℹ Then stage, commit and pull the changes to the repository, to save the changes
## ℹ DO NOT FORGET TO put the new project object into the correct place
## ℹ NEW updated version stored in the workind directory naiv.RData_0.0.8010
## ℹ No validation rules
## ℹ No longitudinal validation rules
example2
## 
## ── naiv project with length = 8 ──
## 
## ℹ  (version 0.0.8010, last update: 2023-11-22)
## ✔ 100 subjects
## ✔ 10 traits
## ✔ In 4 categories
## ℹ Longitudinal data:
## Unbalanced Panel: n = 100, T = 2-17, N = 850
## ✖ naiv project problem with bad_names
```


We observe when we print the object that the alert showing that we have duplicated variables disappear.
When we delete a trait, the method to delete traits, ask for another version number and
a comment to explain the logic for this update.

Its worth to notice also that when we delete a trait, it will be deleted from any category. If
the trait is the only one in one category, also this category is erased. There is no reason
to keep it.


```r
example$categories
## $factors
## [1] "sex"
## 
## $indices
## [1] "tr1"  "tr.3"
## 
## $project1
## [1] "id"   "sex"  "tr+5" "tr2" 
## 
## $project_long
## [1] "incre" "decre" "equ"
example2$categories
## $factors
## [1] "sex"
## 
## $indices
## [1] "tr1"
## 
## $project1
## [1] "id"   "sex"  "tr+5" "tr2" 
## 
## $project_long
## [1] "incre" "decre" "equ"
```


Lets drop three more traits, `tr 4`, `tr+5` and `sex`


```r
example2 <- deleteTraits(example, c("tr 4", "tr+5", "sex"),
                         new_version = c("0.0.8020", "remove 3 more traits"))
## ! Deleting tr 4, tr+5, and sex features.
## ! Traits will also be deleted from factors and project1 categories
## ! Category factors become empty and will be erased
## ℹ Actual version: 0.0.8000
## ℹ No validation rules
## 
## ── Tracking version with `an9elversions` package ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
## 
## ── New version stored in the file:
## • ./to_an9elversions_150d0a5a9511e2.csv
## ℹ Withing the `gitlab` repository of the `an9elversions` package use in R:
## an9elversions::new_track('path_to_file/to_an9elversions_150d0a5a9511e2.csv')
## ℹ Then stage, commit and pull the changes to the repository, to save the changes
## ℹ DO NOT FORGET TO put the new project object into the correct place
## ℹ NEW updated version stored in the workind directory naiv.RData_0.0.8020
## ℹ No validation rules
## ℹ No longitudinal validation rules
example2
## 
## ── naiv project with length = 8 ──
## 
## ℹ  (version 0.0.8020, last update: 2023-11-22)
## ✔ 100 subjects
## ✔ 8 traits
## ✔ In 3 categories
## ℹ Longitudinal data:
## Unbalanced Panel: n = 100, T = 2-17, N = 850
## ✖ naiv project problem with duplicates
## ✖ naiv project problem with bad_names
example2$categories
## $indices
## [1] "tr1"  "tr.3"
## 
## $project1
## [1] "id"  "tr2"
## 
## $project_long
## [1] "incre" "decre" "equ"
```


We can observe that some categories disappear.

## Adding extra data

**Note:** Functions which change the data as "addTraits", "deleteTraits", "updateTraits" will
create another slot called "logs" with the changes in it

In the example2 object we will add some data.


```r
dt_2add <- example[,5:7]
colnames(dt_2add) <- c("tr_3", "tr_4", "tr_5")
traits_2add <- data.frame(trait = colnames(dt_2add),
                          definition = rep("new traits"))
```


If we use this:


```r
example3 <- addTraits(example, dt_2add, traits_2add, new_version = c("0.0.9", "added 3 traits"))
```


it will complain. We need to specify all the columns in the `trait` data.frame


```r
traits_2addNEW <- data.frame(trait = colnames(dt_2add),
                             definition = rep("new traits"),
                             units = "kg",
                             contact = "random people",
                             email = NA)
```

```r
example3 <- addTraits(example,
                      dt_2add,
                      traits_2addNEW,
                      new_version = c("0.0.9", "added 3 traits"))
```


This final step will complain because we do not specify the `id` of each subject.
Lets correct that (but also drop some individuals): We need to introduce the definition
of the new trait `id` also



```r
dt_2add$id <- unlist(example[, "id"])
dt_2add <- dt_2add[-c(4:10), ]
traits_2addNEW <- rbind(traits_2addNEW ,
    c(trait = "id", definition = "mandatory trait", NA, NA, NA))
example3 <- addTraits(example,
                      dt_2add,
                      traits_2addNEW,
                      new_version = c("0.1.0", "added 3 good traits"))
## ℹ Actual version: 0.0.8000
## ℹ No validation rules
## 
## ── Tracking version with `an9elversions` package ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
## 
## ── New version stored in the file:
## • ./to_an9elversions_150d0a1e52bcda.csv
## ℹ Withing the `gitlab` repository of the `an9elversions` package use in R:
## an9elversions::new_track('path_to_file/to_an9elversions_150d0a1e52bcda.csv')
## ℹ Then stage, commit and pull the changes to the repository, to save the changes
## ℹ DO NOT FORGET TO put the new project object into the correct place
## ℹ NEW updated version stored in the workind directory naiv.RData_0.1.0
## ℹ No validation rules
## ℹ No longitudinal validation rules
```

```r
head(example3[], n = 15)
```

<div class="kable-table">

|id    |sex |       tr1| tr2| tr.3|      tr 4|     tr+5| tr_3|      tr_4|     tr_5|
|:-----|:---|---------:|---:|----:|---------:|--------:|----:|---------:|--------:|
|id_1  |F   |  9.730043|   1|    1|   0.00000|       NA|    1|   0.00000|       NA|
|id_2  |M   | 10.073756|   2|    2| 100.58867| 49.99814|    2| 100.58867| 49.99814|
|id_3  |F   | 10.341624|   3|    3| 100.19425| 50.57069|    3| 100.19425| 50.57069|
|id_4  |M   |  9.786925|   4|    4| 100.23101| 49.23016|   NA|        NA|       NA|
|id_5  |M   | 10.231217|   5|    5|  99.69755| 49.52128|   NA|        NA|       NA|
|id_6  |M   |  8.396048|   6|    6| 100.63223| 48.74318|   NA|        NA|       NA|
|id_7  |M   |  9.635677|   7|    7|  99.31478| 50.16288|   NA|        NA|       NA|
|id_8  |M   | 10.225997|   8|    8| 101.94086| 50.03929|   NA|        NA|       NA|
|id_9  |M   | 10.316123|   9|    9|  99.19552| 49.82788|   NA|        NA|       NA|
|id_10 |F   |  8.694974|  10|   10| 100.85224| 48.40917|   NA|        NA|       NA|
|id_11 |M   |  9.921675|  11|   11|  99.54277| 48.74589|   11|  99.54277| 48.74589|
|id_12 |M   | 10.674383|  12|   12| 101.01759| 49.35308|   12| 101.01759| 49.35308|
|id_13 |F   | 10.202928|  13|   13| 100.64418| 49.41192|   13| 100.64418| 49.41192|
|id_14 |M   |  8.291790|  14|   14| 101.33541| 50.08010|   14| 101.33541| 50.08010|
|id_15 |M   |  9.589224|  15|   15| 100.02589| 49.87535|   15| 100.02589| 49.87535|

</div>


Here you can see the difference between the history stored in the object and
The history stored online in the `an9elversions` package. If you do not upload
the change to the `an9elversions` package, someone could do it first and your
changes will be lost.

Only changes from the last version will be allowed



```r
an9elversions::get_versions(example3, online = TRUE)
```

<div class="kable-table">

|time                |project |from_version |to_version |digest                           |sysname |release         |version                              |nodename |machine |login      |user       |effective_user |
|:-------------------|:-------|:------------|:----------|:--------------------------------|:-------|:---------------|:------------------------------------|:--------|:-------|:----------|:----------|:--------------|
|2022-09-20 14:57:32 |naiv    |NA           |0.0.8000   |e4b0fbc3aac52ed8140a62e3bc660337 |Linux   |5.10.0-17-amd64 |n_1_smp_debian_5_10_136_1_2022_08_13 |salambo2 |x86_64  |amartinezp |amartinezp |amartinezp     |

</div>

```r
an9elversions::get_versions(example3, online = FALSE)
```

<div class="kable-table">

|                        |time       |to_version |comment             |project |
|:-----------------------|:----------|:----------|:-------------------|:-------|
|log_2023-11-22_0.1.0    |2023-11-22 |0.1.0      |added 3 good traits |naiv    |
|log_2022-09-20_0.0.8000 |2022-09-20 |0.0.8000   |Initial version     |naiv    |

</div>


Here we can see the diferences in the 2 `an9elversions::get_versions`, one is stored locally with the
comments that can be private, and the public version have information about times, machines
and users.



```r
example3
## 
## ── naiv project with length = 8 ──
## 
## ℹ  (version 0.1.0, last update: 2023-11-22)
## ✔ 100 subjects
## ✔ 14 traits
## ✔ In 4 categories
## ℹ Longitudinal data:
## Unbalanced Panel: n = 100, T = 2-17, N = 850
## ✖ naiv project problem with duplicates
## ✖ naiv project problem with bad_names
```


There are a lot more controls that are invisible here. The changes from the previous version
to the latter, are stored in the 'logs' slot

## Updating a trait

We can update trait characteristics  with `update_trait` function. Let's see how it works:

To update some traits we need the same structure as in the `traits` slot, then for example
to change all the bad names, we could do...



```r
example <- an9elproject:::naiv
bad_names <- an9elproject::check_names(colnames(example))
to_change <- example$traits %>% dplyr::filter(trait %in% bad_names$original_name)
to_change$new_trait <- bad_names$proposed_name
to_change$units <- rep("kg", 3)
to_change$contact <- rep("Sr. Isaac", 3)
```


Here, I change also units and contact. Notice, that is not mandatory to change the name of
the trait, in this case, we need to specify the same name in the `new_trait`


```r
example4 <- update_trait(example, new_df_traits = to_change,
                         new_version = c("0.0.95", "rename some traits and change units"))
## ℹ Actual version: 0.0.8000
## ℹ No validation rules
## 
## ── Tracking version with `an9elversions` package ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
## 
## ── New version stored in the file:
## • ./to_an9elversions_150d0a647b8ecc.csv
## ℹ Withing the `gitlab` repository of the `an9elversions` package use in R:
## an9elversions::new_track('path_to_file/to_an9elversions_150d0a647b8ecc.csv')
## ℹ Then stage, commit and pull the changes to the repository, to save the changes
## ℹ DO NOT FORGET TO put the new project object into the correct place
## ℹ NEW updated version stored in the workind directory naiv.RData_0.0.95
## ℹ No validation rules
## ℹ No longitudinal validation rules
```


This function also works with longitudinal traits, and automatically change the name of the
traits in the corresponding `categories` slot

### practice

* Check that the rename of the trait in `example4` is complete in the tree slots.

---

# Longitudinal data

See the [documentation](longitudinal.md)

# Validate

In the `validate` slot you can put your validation rules using the package
[validate](https://data-cleaning.github.io/validate).



# Testing the package

Finally we need to made some automatic testing to assure that future changes in the source
code integrate correctly with the old one.

Any problems encounter must be coded into a testing unit, for future testing.

To run the testing we use


``` r
covr::gitlab(quiet = FALSE)

```



# isolated the project

We use `renv` package to store the versions of the packages used.

> `renv::init()`

When a new package or a update of a package is successful, we need to call:

> `renv::snapshot()`

If we need to restore the last `snapshot` we use:

> `renv::restore()`

# Export the data of the project

Now there are to functions

* `an9el2plink` allows create a `.ped` file from a  an9elproject object in plink1 and plin2 formats
* `quicktest_filepheno` create a `filepheno` to use in POE analysis with `quicktest`

# SESSION INFO


<details closed>
<summary> <span title='Clik to Expand'> Current session info </span> </summary>

```r

─ Session info ───────────────────────────────────────────────────────────────────────────────────────────────────────────
 setting  value
 version  R version 4.4.2 (2024-10-31)
 os       Ubuntu 24.04.1 LTS
 system   x86_64, linux-gnu
 ui       X11
 language (EN)
 collate  es_ES.UTF-8
 ctype    es_ES.UTF-8
 tz       Europe/Madrid
 date     2025-02-06
 pandoc   3.1.3 @ /usr/bin/ (via rmarkdown)
 quarto   1.6.40 @ /usr/local/bin/quarto

─ Packages ───────────────────────────────────────────────────────────────────────────────────────────────────────────────
 ! package       * version    date (UTC) lib source
 R an9elproject  * 0.9.0      <NA>       [?] <NA>
   an9elutils    * 0.7.1      2025-02-06 [1] gitlab (an9el/an9elutils@13806c4)
 P an9elversions * 0.3.0      2025-02-06 [?] gitlab (an9el/an9elversions@2299a50)
 P anytime         0.3.11     2024-12-19 [?] CRAN (R 4.4.2)
   askpass         1.2.1      2024-10-04 [1] CRAN (R 4.4.2)
   backports       1.5.0      2024-05-23 [1] CRAN (R 4.4.1)
 P base64enc       0.1-3      2015-07-28 [?] CRAN (R 4.4.1)
   bdsmatrix       1.3-7      2024-03-02 [1] CRAN (R 4.4.1)
   bibtex          0.5.1      2023-01-26 [1] CRAN (R 4.4.1)
 P brio            1.1.5      2024-04-24 [?] CRAN (R 4.4.1)
 P cachem          1.1.0      2024-05-16 [?] CRAN (R 4.4.1)
   caret           7.0-1      2024-12-10 [1] CRAN (R 4.4.2)
   checkmate       2.3.2      2024-07-29 [1] CRAN (R 4.4.2)
 P class           7.3-23     2025-01-01 [?] CRAN (R 4.4.2)
 P cli             3.6.3      2024-06-21 [?] CRAN (R 4.4.1)
 P clipr           0.8.0      2022-02-22 [?] CRAN (R 4.4.1)
 P codetools       0.2-20     2024-03-31 [?] CRAN (R 4.4.1)
   collapse        2.0.19     2025-01-09 [1] CRAN (R 4.4.2)
   colorspace      2.1-1      2024-07-26 [1] CRAN (R 4.4.2)
 P commonmark      1.9.2      2024-10-04 [?] CRAN (R 4.4.2)
   data.table    * 1.16.4     2024-12-06 [1] CRAN (R 4.4.2)
 P datawizard      1.0.0      2025-01-10 [?] CRAN (R 4.4.2)
 P desc            1.4.3      2023-12-10 [?] CRAN (R 4.4.1)
   details       * 0.3.0      2022-03-27 [1] CRAN (R 4.4.1)
 P devtools        2.4.5      2022-10-11 [?] CRAN (R 4.4.1)
   digest          0.6.37     2024-08-19 [1] CRAN (R 4.4.2)
   dplyr         * 1.1.4      2023-11-17 [1] CRAN (R 4.4.1)
   DT            * 0.33       2024-04-04 [1] CRAN (R 4.4.1)
 P ellipsis        0.3.2      2021-04-29 [?] CRAN (R 4.4.1)
   evaluate        1.0.3      2025-01-10 [1] CRAN (R 4.4.2)
 P fastmap         1.2.0      2024-05-15 [?] CRAN (R 4.4.1)
   forcats       * 1.0.0      2023-01-29 [1] CRAN (R 4.4.1)
   foreach         1.5.2      2022-02-02 [1] CRAN (R 4.4.1)
   Formula         1.2-5      2023-02-24 [1] CRAN (R 4.4.1)
   fs              1.6.5      2024-10-30 [1] CRAN (R 4.4.2)
   future          1.34.0     2024-07-29 [1] CRAN (R 4.4.2)
   future.apply    1.11.3     2024-10-27 [1] CRAN (R 4.4.2)
   generics        0.1.3      2022-07-05 [1] CRAN (R 4.4.1)
   GGally        * 2.2.1      2024-02-14 [1] CRAN (R 4.4.1)
   ggplot2       * 3.5.1      2024-04-23 [1] CRAN (R 4.4.1)
 P ggstats         0.8.0      2025-01-07 [?] CRAN (R 4.4.2)
   globals         0.16.3     2024-03-08 [1] CRAN (R 4.4.1)
   glue          * 1.8.0      2024-09-30 [1] CRAN (R 4.4.2)
   gower           1.0.2      2024-12-17 [1] CRAN (R 4.4.2)
   gridExtra       2.3        2017-09-09 [1] CRAN (R 4.4.1)
   gtable          0.3.6      2024-10-25 [1] CRAN (R 4.4.2)
   hardhat         1.4.1      2025-01-31 [1] CRAN (R 4.4.2)
   here          * 1.0.1      2020-12-13 [1] CRAN (R 4.4.1)
   hms             1.1.3      2023-03-21 [1] CRAN (R 4.4.1)
 P htmltools       0.5.8.1    2024-04-04 [?] CRAN (R 4.4.1)
 P htmlwidgets     1.6.4      2023-12-06 [?] CRAN (R 4.4.1)
 P httpuv          1.6.15     2024-03-26 [?] CRAN (R 4.4.1)
 P httr            1.4.7      2023-08-15 [?] CRAN (R 4.4.1)
 P insight         1.0.1      2025-01-10 [?] CRAN (R 4.4.2)
 P ipred           0.9-15     2024-07-18 [?] CRAN (R 4.4.1)
   iterators       1.0.14     2022-02-05 [1] CRAN (R 4.4.1)
 P janitor         2.2.1      2024-12-22 [?] CRAN (R 4.4.2)
   jsonlite        1.8.9      2024-09-20 [1] CRAN (R 4.4.2)
   knitcitations * 1.0.12     2021-01-10 [1] CRAN (R 4.4.1)
   knitr         * 1.49       2024-11-08 [1] CRAN (R 4.4.2)
 P later           1.4.1      2024-11-27 [?] CRAN (R 4.4.2)
 P lattice         0.22-6     2024-03-20 [?] CRAN (R 4.4.1)
   lava            1.8.1      2025-01-12 [1] CRAN (R 4.4.2)
 P lifecycle       1.0.4      2023-11-07 [?] CRAN (R 4.4.1)
   listenv         0.9.1      2024-01-29 [1] CRAN (R 4.4.1)
   lmtest          0.9-40     2022-03-21 [1] CRAN (R 4.4.1)
   lubridate     * 1.9.4      2024-12-08 [1] CRAN (R 4.4.2)
   lumberjack    * 1.3.1      2023-03-29 [1] CRAN (R 4.4.1)
 P magick          2.8.5      2024-09-20 [?] CRAN (R 4.4.2)
 P magrittr      * 2.0.3      2022-03-30 [?] CRAN (R 4.4.1)
 P MASS            7.3-64     2025-01-04 [?] CRAN (R 4.4.2)
 P Matrix          1.7-2      2025-01-23 [?] CRAN (R 4.4.2)
 P matrixStats     1.5.0      2025-01-07 [?] CRAN (R 4.4.2)
   maxLik          1.5-2.1    2024-03-24 [1] CRAN (R 4.4.1)
 P memoise         2.0.1      2021-11-26 [?] CRAN (R 4.4.1)
 P mime            0.12       2021-09-28 [?] CRAN (R 4.4.1)
 P miniUI          0.1.1.1    2018-05-18 [?] CRAN (R 4.4.1)
   miscTools       0.6-28     2023-05-03 [1] CRAN (R 4.4.1)
   ModelMetrics    1.2.2.2    2020-03-17 [1] CRAN (R 4.4.1)
   munsell         0.5.1      2024-04-01 [1] CRAN (R 4.4.1)
 P nlme            3.1-167    2025-01-27 [?] CRAN (R 4.4.2)
 P nnet            7.3-20     2025-01-01 [?] CRAN (R 4.4.2)
   openssl         2.3.2      2025-02-03 [1] CRAN (R 4.4.2)
   pacman        * 0.5.1      2019-03-11 [1] CRAN (R 4.4.1)
   pander          0.6.5      2022-03-18 [1] CRAN (R 4.4.1)
   parallelly      1.42.0     2025-01-30 [1] CRAN (R 4.4.2)
   pillar          1.10.1     2025-01-07 [1] CRAN (R 4.4.2)
 P pkgbuild        1.4.6      2025-01-16 [?] CRAN (R 4.4.2)
 P pkgconfig       2.0.3      2019-09-22 [?] CRAN (R 4.4.1)
 P pkgload         1.4.0      2024-06-28 [?] CRAN (R 4.4.1)
 P plm             2.6-5      2025-01-17 [?] CRAN (R 4.4.2)
   plyr          * 1.8.9      2023-10-02 [1] CRAN (R 4.4.1)
   png             0.1-8      2022-11-29 [1] CRAN (R 4.4.1)
   pROC            1.18.5     2023-11-01 [1] CRAN (R 4.4.1)
   prodlim         2024.06.25 2024-06-24 [1] CRAN (R 4.4.1)
 P profvis         0.4.0      2024-09-20 [?] CRAN (R 4.4.2)
 P promises        1.3.2      2024-11-28 [?] CRAN (R 4.4.2)
   pryr            0.1.6      2023-01-17 [1] CRAN (R 4.4.1)
   purrr         * 1.0.4      2025-02-05 [1] CRAN (R 4.4.2)
   R.methodsS3     1.8.2      2022-06-13 [1] CRAN (R 4.4.1)
 P R.oo            1.27.0     2024-11-01 [?] CRAN (R 4.4.2)
   R.utils         2.12.3     2023-11-18 [1] CRAN (R 4.4.1)
 P R6              2.5.1      2021-08-19 [?] CRAN (R 4.4.1)
   rapportools     1.1        2022-03-22 [1] CRAN (R 4.4.1)
 P rbibutils       2.3        2024-10-04 [?] CRAN (R 4.4.2)
   RColorBrewer    1.1-3      2022-04-03 [1] CRAN (R 4.4.1)
   Rcpp            1.0.14     2025-01-12 [1] CRAN (R 4.4.2)
 P Rdpack          2.6.2      2024-11-15 [?] CRAN (R 4.4.2)
   readr         * 2.1.5      2024-01-10 [1] CRAN (R 4.4.1)
 P recipes         1.1.0      2024-07-04 [?] CRAN (R 4.4.1)
   RefManageR      1.4.0      2022-09-30 [1] CRAN (R 4.4.1)
 P remotes         2.5.0      2024-03-17 [?] CRAN (R 4.4.1)
 P renv            1.1.0      2025-01-29 [?] CRAN (R 4.4.2)
   reshape2        1.4.4      2020-04-09 [1] CRAN (R 4.4.1)
 P rio             1.2.3      2024-09-25 [?] CRAN (R 4.4.2)
   rlang           1.1.5      2025-01-17 [1] CRAN (R 4.4.2)
   rlog            0.1.0      2021-02-24 [1] CRAN (R 4.4.1)
   rmarkdown     * 2.29       2024-11-04 [1] CRAN (R 4.4.2)
 P roxygen2        7.3.2      2024-06-28 [?] CRAN (R 4.4.1)
 P rpart           4.1.24     2025-01-07 [?] CRAN (R 4.4.2)
 P rprojroot       2.0.4      2023-11-05 [?] CRAN (R 4.4.1)
 P rstudioapi      0.17.1     2024-10-22 [?] CRAN (R 4.4.2)
 P sandwich        3.1-1      2024-09-15 [?] CRAN (R 4.4.2)
   scales          1.3.0      2023-11-28 [1] CRAN (R 4.4.1)
 P sessioninfo     1.2.3      2025-02-05 [?] CRAN (R 4.4.2)
 P shiny           1.10.0     2024-12-14 [?] CRAN (R 4.4.2)
   snakecase       0.11.1     2023-08-27 [1] CRAN (R 4.4.1)
 P stringi         1.8.4      2024-05-06 [?] CRAN (R 4.4.1)
 P stringr       * 1.5.1      2023-11-14 [?] CRAN (R 4.4.1)
   summarytools  * 1.0.1      2022-05-20 [1] CRAN (R 4.4.1)
 P survival        3.8-3      2024-12-17 [?] CRAN (R 4.4.2)
 P testthat      * 3.2.3      2025-01-13 [?] CRAN (R 4.4.2)
 P tibble        * 3.2.1      2023-03-20 [?] CRAN (R 4.4.1)
   tidyr         * 1.3.1      2024-01-24 [1] CRAN (R 4.4.1)
   tidyselect      1.2.1      2024-03-11 [1] CRAN (R 4.4.1)
   tidyverse     * 2.0.0      2023-02-22 [1] CRAN (R 4.4.1)
   timechange      0.3.0      2024-01-18 [1] CRAN (R 4.4.1)
   timeDate        4041.110   2024-09-22 [1] CRAN (R 4.4.2)
 P tsibble       * 1.1.6      2025-01-30 [?] CRAN (R 4.4.2)
   tzdb            0.4.0      2023-05-12 [1] CRAN (R 4.4.1)
 P urlchecker      1.0.1      2021-11-30 [?] CRAN (R 4.4.1)
 P usethis         3.1.0      2024-11-26 [?] CRAN (R 4.4.2)
 P utf8            1.2.4      2023-10-22 [?] CRAN (R 4.4.1)
 P vctrs           0.6.5      2023-12-01 [?] CRAN (R 4.4.1)
   vetr            0.2.18     2024-06-21 [1] CRAN (R 4.4.1)
   viridis         0.6.5      2024-01-29 [1] CRAN (R 4.4.1)
   viridisLite     0.4.2      2023-05-02 [1] CRAN (R 4.4.1)
   visNetwork      2.1.2      2022-09-29 [1] CRAN (R 4.4.1)
   withr         * 3.0.2      2024-10-28 [1] CRAN (R 4.4.2)
 P writexl         1.5.1      2024-10-04 [?] CRAN (R 4.4.2)
   xfun            0.50       2025-01-07 [1] CRAN (R 4.4.2)
 P xml2            1.3.6      2023-12-04 [?] CRAN (R 4.4.1)
 P xtable          1.8-4      2019-04-21 [?] CRAN (R 4.4.1)
   yaml            2.3.10     2024-07-26 [1] CRAN (R 4.4.2)
   zoo             1.8-12     2023-04-13 [1] CRAN (R 4.4.1)

 [1] /home/amartinezp/.cache/R/renv/library/an9elproject-6479f886/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu
 [2] /home/amartinezp/.cache/R/renv/sandbox/linux-ubuntu-noble/R-4.4/x86_64-pc-linux-gnu/9a444a72

 * ── Packages attached to the search path.
 P ── Loaded and on-disk path mismatch.
 R ── Package was removed from disk.

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

```

</details>
<br>

