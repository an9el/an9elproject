# an9elproject 0.10.0

# an9elproject 0.9.0

* Added function to delete longitudinal observations `deleteObservations`
* Corrected an error when merging p.data.frames with factors and new levels
* Added the function `ldescribe` is like summary for pdata.frames
* Corrected an error when addTraits which elements are lists

# an9elproject 0.8.1

# an9elproject 0.8.0

# an9elproject 0.7.2

# an9elproject 0.7.1

* Add help with the man method

# an9elproject 0.7.0

* Added validation slot and function with the validate package

# an9elproject 0.6.0

* Added support for add new temporal data with update.values
* Update_values now fail if detect new subjects or traits. It needed to update_traits first
* Initialize the log_level to 3 on load the package

# an9elproject 0.5.0

* Added log system with rlog

# an9elproject 0.4.0

* Added export to plink and export to quicktest

# an9elproject 0.3.0

# an9elproject 0.2.2

# an9elproject 0.2.1

* Corrected an error that do not delete old warnings when update the object
* `Update_version` now assure that "data" and "traits" are tibbles
* start the change to use `collapse` package to improve speed
* Added function to `describe` the data

# an9elproject 0.2.0

* Added internal function `toLogger` to log the changes
* Added support for logging the changes with the `lumberjack` package in the logs slot
* Added dependency "Depends" on `lumberjack`
* Added function to update values of a feature
* Added function to check the project and put the results in the `warnings` slot
* Change the behaviour of `print` method to be faster.
* Added function to
* `addTraits`, `deleteTraits`, `update_trait`, `findTraits` and `update_values` now are functions,
not methods, it means that now we can call `help("addTraits")`, for example
* Added method for `.[` to access the `data` slot
* Increase the test coverage
* Added function `update_trait` to change features definitions. This allow to change
all the wrong names with one command.
* Add functionality in `deleteTraits` to delete all missing features with one command
just run the function without `traits` parameter.
* Remove `history_version` an integrate its funcionality within `an9elversions::get_versions`
function



# an9elproject 0.1.0

* Start to adding support for longitudinal data (features measured over time)
* `update.version` now includes from which version to whom version. 
* HASH support included to know that no modifications were made
* Added support for `misc` slot to include all types of miscelanea data

# an9elproject 0.0.0.9003

* Added a `NEWS.md` file to track changes to the package.
* Added `update_trait` function and the test for that
* Remove `variables.names`, `cases.names` method as they are now `colnames` and `row.names`
* Now the slot `data` have row.names to allow the method `row.names` to retrieve what we need
