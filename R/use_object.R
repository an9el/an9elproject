
#' Find duplicates variable in a data.frame or a an9elproject object
#'
#' This function try to improve the results of \code{duplicated.matrix(obj, MARGIN = 2)} translating to character the variables first. It return a data.frame with the columns
#' duplicated in the given order and in the reverse order (rev). Rownames are ordered in
#' alphabetical order
#' @keywords internal
#' @param obj object of class \emph{data.frame} or \emph{an9elproject}
#' @export
findDuplicates <- function(obj) {
    UseMethod("findDuplicates", obj)
}

#' @rdname findDuplicates
#' @export
#' @param obj object of class \emph{data.frame} or \emph{an9elproject}
#' @keywords internal
findDuplicates.default <- function(obj) {
    cli::cli_alert_warning("Method not implemented for this class")
    return(invisible())
}

#' @rdname findDuplicates
#' @export
#' @param obj object of class \emph{data.frame} or \emph{an9elproject}
#' @keywords internal
findDuplicates.data.frame <- function(obj) {
    rlog::log_trace("findDuplicates: Finding duplicates")
    duplicate <- obj %>%
        collapse::ftransformv(colnames(.), as.character) %>%
        duplicated.matrix(MARGIN = 2)
    rlog::log_trace("findDuplicates: Finding reverse duplicates")
    duplicate_rev <- obj[, ncol(obj):1] %>%
        collapse::ftransformv(colnames(.), as.character) %>%
        duplicated.matrix(MARGIN = 2)
    repes <- cbind(duplicate = duplicate, duplicate_rev = rev(duplicate_rev))
    repes <- repes[rowSums(repes)> 0, ]
    if (nrow(repes)) {
        rlog::log_info("findDuplicates: Found features that coincide in all samples")
        repes <- repes[order(rownames(repes)), ]
    }
    return(repes)
}

#' @rdname findDuplicates
#' @export
#' @param obj object of class \emph{data.frame} or \emph{an9elproject}
#' @keywords internal
findDuplicates.an9elproject <- function(obj) {
    repes <- findDuplicates(obj$data)
    return(repes)
}



#' Descriptive features. Wrapper for descr of the collapse package
#'
#' wrapper for descr of the collapse package
#' @title descriptives of some features
#' @param obj object of class \emph{an9elproject}
#' @param features names of the features to describe
#' @param subjects id of the subject used for the descriptives.
#' @return tibble with the description of each feature
#' @examples
#' obj <- an9elproject:::naiv
#' describe(obj, colnames(obj)[1:4])
#' @author
#' person("Angel", "Martinez-Perez",
#' email = \email{angelmartinez@protonmail.com},
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#' 
#' @export
describe <- function(obj, features, subjects) {
    rlog::log_trace("describe: init")

    checkmate::assertMultiClass(obj, c("an9elproject", "list"))
    if (missing(subjects)) {
        rlog::log_debug("describe: No filter pased, descritives for all subjects returned")
        subjects <- obj$data$id
    } else if (class(subjects) != "integer") {
        rlog::log_debug("describe: subjects not integer")
        checkmate::assertSubset(subjects, choices = obj$data$id)
    }
    if (missing(features)) {
        rlog::log_warn("describe: Returning descriptives of all features")
        features <- colnames(obj$data)
    } else {
        checkmate::assertSubset(features, choices = colnames(obj$data))
    }

    ret <- obj[subjects, features] %>%
        collapse::descr(Qprobs = c(0.05, 0.5, 0.95)) %>%
        tibble::as_tibble()
    return(ret)
}


#' summary for pdata.frames
#'
#' summary for pdata.frames
#' @title summary for pdata.frames
#' @param obj object of class \emph{pdata.frame}
#' @return data.frame with the summary of each longitudinal feature
#' @examples
#' obj <- an9elproject:::naiv
#' ldescribe(long(obj))
#' @author
#' person("Angel", "Martinez-Perez",
#' email = \email{angelmartinez@protonmail.com},
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#' 
#' @export
ldescribe  <- function(obj) {

  sum_per_column <- function(obj) {
    class(obj) <- setdiff(class(obj), "pseries")
    res <- collapse::qsu(data.frame(obj))[1, , drop = FALSE] %>%
      collapse::qDF(row.names.col = "variable") %>% 
      collapse::fselect(variable, n = N, mean = Mean, sd = SD) %>% 
      dplyr::mutate(percent_missing = 100 * (1 - n/length(obj)))
    res <- res %>% dplyr::select(-variable)
    return(res)
  }

  ressum <- plyr::ldply(obj, sum_per_column)
  return(ressum)
}


#' Return the name of the kinship between a pairs of individuals
#'
#' Given an an9elproject object return the type of kinship between
#' a pair of individuals
#' @param obj object of class \emph{data.frame} or \emph{an9elproject}
#' @param ... other parameters, usually the ids
#' @export
kinship_name <- function(obj, ...) {
    UseMethod("kinship_name", obj)
}


#' @rdname kinship_name
#' @export
#' @param obj object of class \emph{an9elproject}
#' @param ... other parameters, usually the ids
#' @keywords internal
kinship_name.default <- function(obj, ...) {
    cli::cli_alert_warning("Method not implemented for this class")
    return(invisible())
}

#' @rdname kinship_name
#' @export
#' @param obj object of class \emph{an9elproject}
#' @param id1 id of the first subject
#' @param id2 id of the second subject
#' @keywords internal
kinship_name.an9elproject <- function(obj, id1, id2) {
    rlog::log_trace("kinship_name: init")

    checkmate::assertCharacter(id1, len = 1)
    checkmate::assertCharacter(id2, len = 1)
    
    fam <- "FAM" # name of the family field
    fa <- "FA"   # name of the father field
    mo <- "MO"   # name of the mother field

    ##  id1 <- "32205"
    ##  id2 <- "32209"

    
    if (!all(c(fam, fa, mo) %in% colnames(obj))) {
        mess <- paste0("Not ", fam, ", ", fa, "and ", mo," features in the object")
        return(mess)
    }

    nuclear <- obj[, c("id", fam, fa, mo)] %>% dplyr::filter(id %in% c(id1, id2))
    if (any(is.na(nuclear[,2:4]))) {
        return("Some parental information is missing")
    }
    if ((nrow(nuclear) != 2) || (nuclear[1,2] != nuclear[2,2])) {
        return("none")
    }

    ascendentes <- unique(unlist(nuclear[, 3:4]))
    
    if (sum(c(id1, id2) %in% ascendentes) == 1) {
        return("parent-offspring")
    }

    if (all(ascendentes == "0")) {
        return("none")
    }
    return("not implemented yet")
    
}


