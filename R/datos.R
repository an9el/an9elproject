#' Example Dataset: dt_naive
#'
#' This dataset contains simulated data for demonstration purposes.
#'
#' @format A tibble (data.frame) with 100 rows and 7 columns:
#' \describe{
#'   \item{id}{Character. Unique identifier for each individual (e.g., "id_1", "id_2").}
#'   \item{sex}{Factor. Sex of the individual (levels: "M", "F").}
#'   \item{tr1}{Numeric. A continuous trait.}
#'   \item{tr2}{Integer. A discrete trait.}
#'   \item{tr.3}{Integer. Another discrete trait.}
#'   \item{tr 4}{Numeric. Another continuous trait.}
#'   \item{tr+5}{Numeric. A continuous trait, possibly with missing values.}
#' }
#'
#' @usage data(dt_naive)
#'
#' @examples
#' data(dt_naive)
#' head(dt_naive)
"dt_naive"

#' Example Dataset: traits_naive
#'
#' This dataset contains metadata for different traits, including their definitions,
#' measurement units, and contact information for data inquiries.
#'
#' @format A tibble (data.frame) with 11 rows and 5 columns:
#' \describe{
#'   \item{trait}{Character. The name of the trait (e.g., "tr1", "tr2").}
#'   \item{definition}{Character. A brief description of the trait.}
#'   \item{units}{Character. Measurement units of the trait (e.g., "kg", "m", or "NA" if not applicable).}
#'   \item{contact}{Character. Name of the researcher responsible for the trait information.}
#'   \item{email}{Character. Contact email for inquiries regarding the trait data.}
#' }
#'
#' @usage data(traits_naive)
#'
#' @examples
#' data(traits_naive)
#' head(traits_naive)
"traits_naive"



#' Example Dataset: dt_long
#'
#' This dataset contains time-related measurements for various individuals.
#'
#' @format A tibble (data.frame) with 850 rows and 5 columns:
#' \describe{
#'   \item{id}{Character. Unique identifier for each individual.}
#'   \item{id_long}{Date. Timestamp associated with each measurement.}
#'   \item{incre}{Numeric. Value indicating an increasing trend.}
#'   \item{decre}{Numeric. Value indicating a decreasing trend.}
#'   \item{equ}{Numeric. Equilibrium-related measurement.}
#' }
#'
#' @usage data(dt_long)
#'
#' @examples
#' data(dt_long)
#' head(dt_long)
"dt_long"


#' Example Dataset: categ_naive
#'
#' This dataset is a list containing categorized variable groupings for analysis.
#'
#' @format A list with four elements:
#' \describe{
#'   \item{factors}{Character vector. Contains categorical variables (e.g., "sex").}
#'   \item{indices}{Character vector. Contains index variables for analysis (e.g., "tr1", "tr.3").}
#'   \item{project1}{Character vector. Variables included in project 1 (e.g., "id", "sex", "tr+5", "tr2").}
#'   \item{project_long}{Character vector. Variables included in long-format project (e.g., "incre", "decre", "equ").}
#' }
#'
#' @usage data(categ_naive)
#'
#' @examples
#' data(categ_naive)
#' str(categ_naive)
"categ_naive"

#' Proyecto naive (datos de demostración)
#'
#' Objeto de clase [an9elproject] que contiene datos aleatorios creados con fines demostrativos.
#' Este proyecto incluye:
#' 
#' - 100 sujetos.
#' - 11 rasgos.
#' - Datos organizados en 4 categorías.
#' - Datos longitudinales en un panel no balanceado (n = 100, T = 3-15, N = 850).
#'
#' Además, se han identificado dos problemas en el conjunto de datos:
#' 
#' - **Duplicados:** Existen registros duplicados.
#' - **Nombres incorrectos:** Algunos nombres de variables no cumplen con las convenciones esperadas.
#'
#' @details
#' El objeto `naive` se creó para ilustrar las capacidades del objeto de clase `an9elproject`. La impresión del objeto muestra
#' un resumen de sus características principales, incluyendo la versión, la fecha de última actualización, la cantidad de sujetos,
#' rasgos, categorías y detalles de la estructura longitudinal de los datos.
#'
#' @format Un objeto de clase `an9elproject` que se imprime de la siguiente forma:
#' \preformatted{
#' ── naive project with length = 8 ──
#'
#' ℹ  (version 0.0.8000, last update: 2025-02-06)
#' ✔ 100 subjects
#' ✔ 11 traits
#' ✔ In 4 categories
#' ℹ Longitudinal data:
#' Unbalanced Panel: n = 100, T = 3-15, N = 850
#' ✖ naive project problem with duplicates
#' ✖ naive project problem with bad_names
#' }
#'
#' @source Datos generados aleatoriamente para fines demostrativos.
#'
#' @examples
#' # Mostrar un resumen del proyecto
#' print(naive)
#'
#' # Acceder a la versión del proyecto
#' naive$version
"naive"
