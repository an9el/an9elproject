
#' Add features or temporal features in a \code{an9elproject} object
#'
#' Add features or temporal features in a \code{an9elproject} object
#' @param obj object of class \emph{an9elproject}
#' @param dt data.frame or similar with the data to incorporate to the project. See \code{details}
#' @param traits data.frame with the description of the features for example \code{forceNames}
#' @param forceNames to force include bad names
#' @param ... others parameters to pass to \code{.update_version}
#' @details
#' If the data.frame to introduce have a column name called \code{id_long}, it will be
#' assumed that the data to introduce is purely temporal data, if not, then is supposed
#' to be purely normal features.
#' @examples
#' obj <- an9elproject:::naiv
#' dt2 <- (obj[] %>% dplyr::transmute(tr4 = tr2 * 2, id = id, tr.5 = tr1 + tr2))[-c(2:3),]
#' traits <- obj$traits[1:3,]
#' traits$trait <- c("id", "tr4", "tr.5")
#' traits$definition <- c("add test1", "addTest2", "addTest3")
#' obj2 <- addTraits(obj, dt2, traits, forceNames = TRUE, new_version = c("0.9", "addTraits test"))
#' dt3 <- obj$longitudinal %>% dplyr::transmute(id = id, id_long = id_long, testLong = incre - decre)
#' traits$trait <- c("id", "id_long", "testLong")
#' dt3 <- dt3[-c(4:10),]
#' obj3 <- addTraits(obj, dt3, traits, forceNames = TRUE, new_version = c("0.95", "addTraits long test"))
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
addTraits <- function(obj, dt, traits, forceNames = FALSE, ...) {
    checkmate::assertMultiClass(obj, c("an9elproject", "list"))
    ## check if version is the latest one
    checkmate::assertTRUE(an9elversions::check_last_version_online(obj))
    obj$logs <- NULL
    checkmate::assertFlag(forceNames)
    if (!forceNames) {
        rlog::log_debug("addTraits: forcing Names")
        checkmate::assertSetEqual(nrow(an9elproject::check_names(traits$trait)), 0)
    }
    checkmate::assertDataFrame(dt, col.names = "unique")
    checkmate::assertTRUE(is.element(el = "id", set = colnames(dt)))
    ## check that there aren't new subjects
    checkmate::assertTRUE(all(dt$id %in% rownames(obj)))
    
    ## check all trait description columns exists
    indexes <- unique(c("id", names(lindex(obj))))
    checkmate::assertSetEqual(colnames(traits) , colnames(obj$traits))
    ## check if the features already exists, except for the id or temporal_id
    checkmate::assertFALSE(any(obj$traits$trait %in%
                               (setdiff(colnames(dt), indexes))))

    
    ## update the traits description
    rlog::log_debug("addTraits: traits description")
    traitsNoId <- traits %>%
        collapse::fsubset(!(trait %in% indexes))
    new <- obj$traits %>% rbind.data.frame(traitsNoId) %>%
        collapse::ftransform(new_trait = trait)
    checkmate::assertSetEqual(anyDuplicated(new$trait), 0)
    toLog <- toLogger(obj$traits, new, type = "traits")
    obj$traits <- obj$traits %>% rbind.data.frame(traitsNoId)

    has_long <- has_long_data(obj, dt)
    if (has_long) {
        rlog::log_debug("addTraits: has long")
        new <- merge(obj$longitudinal, dt)
        toLog2 <- toLogger(obj$longitudinal, new, type = "longitudinal")
        obj$longitudinal <- new 
    } else {
        new <- obj$data %>% dplyr::full_join(dt, by = "id")        
        toLog2 <- toLogger(obj$data, new)
        obj$data <- new
    }
    
    toLog <- rbind.data.frame(toLog, toLog2)
    toLog$expression <- testthat::capture_output(print(match.call()))
    obj$logs <- toLog

    obj <- .update_version(obj, ...)
    return(obj)
}


#' Delete features in a \code{an9elproject} object
#'
#' Delete features and remove the feature names from the categories 
#' @param obj object of class \emph{an9elproject}
#' @param traits name of the features to delete. If missing delete features
#' with all missing values
#' @param verbose logical to print information or not
#' @param ... optional parameter "new_version" of the \code{.update_version} function
#' @details the features can be temporal or not. If missing `traits`, then all features
#' with all missing values are deleted. This is a nice function to delete missing
#' features for all subjects.
#' @examples
#' obj <- an9elproject:::naiv
#' obj2 <- deleteTraits(obj, traits = c("tr.3", "incre"), new_version = c("0.9", "deleteTraits test"))
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
deleteTraits <- function(obj, traits, verbose = TRUE, ...) {
    checkmate::assertMultiClass(obj, c("an9elproject", "list"))
    ## check if version is the latest one
    checkmate::assertTRUE(an9elversions::check_last_version_online(obj))
    checkmate::assertFlag(verbose)
    if (missing(traits)) {
        rlog::log_debug("deleteTraits: remove features with all missing")
        traitsvar <- collapse::varying(obj[])
        if (!(is.null(obj$longitudinal))) {
            traitsvar <- collapse::varying(obj$longitudinal) %>% c(traitsvar)
        }        
        traits <- traitsvar %>% collapse::get_vars(!traitsvar, return = "names")
    }
    
    indexes <- unique(c("id", names(lindex(obj))))
    traits <- traits %>% collapse::funique() %>% setdiff(indexes)
    if (length(traits) < 1) {
        rlog::log_warn("deleteTraits: No traits to delete")
        cli::cli_alert_info("No available features to delete")
        return(invisible())
    }
    
    checkmate::assertSubset(traits, obj$traits$trait)
    if (verbose) {
        cli::cli_div(theme = list(span.emph = list(color = "red")))
        cli::cli_alert_warning("Deleting {.emph {traits}} featur{?e/es}.")
    }
    
    toLog <- data.frame(time = NA, key = NA, variable = NA, old = NA, new = NA)
    toLog <- toLog[-1,]
    delete_normal <- intersect(traits, colnames(obj$data))
    if (length(delete_normal) > 0) {
        new <- obj$data %>% dplyr::select(-all_of(delete_normal))
        toLog <- toLog %>% rbind.data.frame(toLogger(obj$data, new, type = "data"))
        obj$data <- new
    }
    new <- obj$traits %>%
        collapse::fsubset(!(trait %in% traits)) %>%
        dplyr::mutate(new_trait = trait)
    toLog2 <- toLogger(obj$traits, new, type = "traits")
    obj$traits <- obj$traits %>%
        collapse::fsubset(!(trait %in% traits))
    toLog <- toLog %>% rbind.data.frame(toLog2)
    
    if (!is.null(obj$longitudinal)) {
        delete_long <- intersect(traits, colnames(obj$longitudinal))
        if (length(delete_long) > 0) {
            rlog::log_debug("deleteTraits: remove traits")
            new <- obj$longitudinal %>% dplyr::select(-all_of(delete_long))
            toLog3 <- toLogger(obj$longitudinal, new, type = "longitudinal")
            toLog <- toLog %>% rbind.data.frame(toLog3)
            obj$longitudinal <- new
        }
    }
    
    dropFromCategory <- obj$categories %>%
        purrr::map_lgl(function(x)  any(traits %in% x))
    if (any(dropFromCategory)) {
        old_category <- obj$categories
        rlog::log_debug("deleteTraits: remove traits from categories")        
        dfCat <- names(dropFromCategory)[dropFromCategory]
        if (verbose) {
            cli::cli_div(theme = list(span.emph = list(color = "red")))
            cli::cli_alert_warning("Traits will also be deleted from {.emph {dfCat}} categor{?y/ies}")
        }
        for (i in dfCat) {
            obj$categories[[i]] <- setdiff(obj$categories[[i]], traits)
            if (length(obj$categories[[i]]) == 0) {
                if (verbose) {
                    cli::cli_alert_warning("Category {.emph {i}} become empty and will be erased")
                }
                obj$categories[[i]] <- NULL
            }            
        }
        toLog_category <- toLogger(logger_list(old_category, type = "category"),
                    logger_list(obj$categories, type = "category"),
                    type = "category")
        toLog <- toLog %>% rbind.data.frame(toLog_category)
    }
    toLog$expression <- testthat::capture_output(print(match.call()))
    obj$logs <- toLog
    obj <- .update_version(obj, ...)
    return(obj)
}


#' Update version of an \code{an9elproject} object
#'
#' Update version of an \code{an9elproject} object
#' @param obj object of class \emph{an9elproject}
#' @param new_version character of length 2 with the new version and the comments
#' 
#' @keywords internal
.update_version <- function(obj, new_version) {
    checkmate::assertClass(obj, c("an9elproject", "list"))
    rlog::log_trace(".update_version: init")
    newest_version_online  <- an9elversions::get_versions(obj)$to_version[1]
    if (!identical(obj$version$version, newest_version_online)) {
        rlog::log_fatal(".update_version: update from old version")        
        cli::cli_div(theme = list(span.emph = list(color = "red")))
        cli::cli_alert_info("Actual version: {.emph {obj$version$version}}")
        stop("You can not update and old version, newer version exists")
    }        
    
    obj$data <- obj$data %>% tibble::as_tibble(rownames = NA)
    obj$traits <- obj$traits %>% tibble::as_tibble(rownames = NA)
    suppressWarnings(rownames(obj$data) <- obj$data$id)
    old_version <- obj$version$version
    cli::cli_div(theme = list(span.emph = list(color = "red")))
    cli::cli_alert_info("Actual version: {.emph {obj$version$version}}")

    if (missing(new_version)) {
        rlog::log_trace(".update_version: ask for new version")
        cli::cli_alert_info("Introduce the new version, press INTRO and insert a log to explain what change")
        new_version <- scan(what = c("character", "character"),
                            nmax = 2, sep = "#", multi.line = FALSE)
    }
    
    checkmate::assertCharacter(new_version, len = 2)
    obj$version$version <- new_version[1]
    hoy <- Sys.Date()
    obj$version[[paste0("log_", hoy, "_", new_version[1])]] <- new_version[2]

    ## warnings
    obj <- check_project(obj, interactive = FALSE)
    ## new data
    obj$version$date <- Sys.Date()
    
    ## This is to update the version tracker, with the package "an9elversions"
    name <- obj$version$project
    time <- lubridate::now("CET")
    version <- obj$version$version
    digest <- an9elversions::check_hash(obj)
    if (is.logical(digest)) {
        rlog::log_fatal(".update_version: new version exists")
        cli::cli_alert_info("This version ({name}, {version}) already exists")
        return(invisible())
    }
    other <- Sys.info()
    other['version'] <- other['version'] %>% janitor::make_clean_names(replace = c(`#` = "n_"))
    tracking <- data.frame(
        time,
        project = name,
        from_version = old_version,
        to_version = version,
        digest = digest,
        t(other))
    tracking_file <- tempfile(pattern = "to_an9elversions_", tmpdir = ".", fileext = ".csv")
    rlog::log_trace(".update_version: writing tracking file")
    data.table::fwrite(tracking,
       file = tracking_file,
       append = FALSE,
       quote = "auto",
       row.names = FALSE,
       col.names = FALSE)
    codigo <- glue::glue("an9elversions::new_track('path_to_file/{stringr::str_sub(tracking_file, start = 3)}')")
    cli::cli_div(theme = list(span.emph = list(color = "red")))
    cli::cli_h1("{.emph Tracking version with `an9elversions` package}")
    cli::cli_h3("New version stored in the file:")
    cli::cli_bullets(c("*" = {tracking_file}))
    cli::cli_alert_info("Withing the `gitlab` repository of the `an9elversions` package use in R:")
    cli::cli_code(codigo)
    cli::cli_alert_info("Then stage, commit and pull the changes to the repository, to save the changes")
    cli::cli_alert_info("DO NOT FORGET TO put the new project object into the correct place")
    new_version_name <- paste0(obj$version$project, ".RData_", obj$version$version)
    cli::cli_alert_info("NEW updated version stored in the workind directory {.emph {new_version_name}}")
    validate(obj)
    validate(obj, long = TRUE)
    check_changes(obj)
    rlog::log_trace(".update_version: writing project file")
    save(obj, file = new_version_name)

    return(obj)
}


#' Update features of an \code{an9elproject} object
#'
#' Update features of an \code{an9elproject} object. The `new_df_traits` must be similar to
#' `obj@traits` but with one column more called `new_trait`. The `new_df_traits$trait` must
#' be the same as in `obj@traits$trait`, you can change the rest of the columns.
#'
#' @param obj object of class \emph{an9elproject}
#' @param new_df_traits data.frame as in `obj@traits` with one column more called `new_trait`
#' @param ... optional \code{comments = c("new_version", "comment of changes"))}
#' @examples
#' obj <- an9elproject:::naiv
#' newt <- obj$traits %>% collapse::fsubset(trait %in% c("tr.3", "incre")) %>% data.frame
#' newt[1,2:3] <- c("good name for tr.3", "kg")
#' newt[2,2:3] <- c("salto", "m")
#' newt$new_trait <- c("tr_3", "incremento")
#' obj2 <- update_trait(obj, new_df_traits = newt, new_version = c("1", "update_trait test"))
#'
#' @details If you are not using the last version of the project, you can not update features.
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
update_trait <- function(obj, new_df_traits, ...) {
    rlog::log_trace("update_trait: init")
    checkmate::assertMultiClass(obj, c("an9elproject", "list"))
    ## check if version is the latest one
    checkmate::assertTRUE(an9elversions::check_last_version_online(obj))
    ## check for correct type in new_df_traits
    vetr::vet(cbind(obj$traits, new_trait = 'a')[0,], new_df_traits)
    indexes <- unique(c("id", names(lindex(obj))))
    ## make sure that we do not change the names of the indexes, allowed to change definition but not name here:
    if (any(new_df_traits$new_trait %in% obj$traits$trait)) {
        checkmate::assertTRUE(all(new_df_traits$trait == new_df_traits$new_trait))
    }
    
    checkmate::assertSubset(new_df_traits$trait, choices = obj$traits$trait)
    new <- obj$traits %>%
        collapse::ftransform(new_trait = trait) %>%
        collapse::fsubset(!(trait %in% new_df_traits$trait)) %>%
        rbind.data.frame(new_df_traits)
    rlog::log_trace("update_trait: toLogger")
    toLog <- toLogger(obj$traits, new, type = "traits")
 

    name2change <- new_df_traits$trait != new_df_traits$new_trait
    old_names <- new_df_traits$trait[name2change]
    new_names <- new_df_traits$new_trait[name2change]
    ## if we change some features id, we have to change the categories, the trait, the data and
    ## the longitudinal data.frames, if not, just change the trait data.frame
    if (collapse::anyv(name2change, TRUE)) {
        ## check that the new names are good names
        new_names_checked <- janitor::make_clean_names(new_names, case = "parsed") %in% new_names
        if (collapse::anyv(new_names_checked, FALSE)) {
            bad_names <- new_names[!new_names_checked]
            rlog::log_fatal("update_trait: Names not clean")
            cli::cli_div(theme = list(span.emph = list(color = "red")))
            cli::cli_alert_warning("I refuse to insert bad feature names into the BD ({.emph {bad_names}})")
            return(invisible())
        }
        total_names <- c(setdiff(obj$traits$trait, old_names), new_names)
        ## check that we do not introduce any new duplicated in the id of features
        checkmate::assertTRUE(collapse::allv(duplicated(total_names), FALSE))
        
        ## update categories, only if some features id changed
        replace_one <- function(vec, old_feature_name, new_feature_name) {
            vec <- collapse::copyv(vec, old_feature_name, new_feature_name)
            return(vec)
        }
        ## recursively replacement of names
        if (length(obj$categories) > 0) {
            old_category <- obj$categories
            for (i in seq_along(new_names)) {
                rlog::log_info(paste("update_trait: Updating categories. Replacing",
                                     old_names[i], "for", new_names[i]))
                obj$categories <- obj$categories %>%
                    collapse::rapply2d(replace_one,
                                       old_names[i],
                                       new_names[i])
            }
            toLog_category <- toLogger(logger_list(old_category, type = "category"),
                                       logger_list(obj$categories, type = "category"),
                                       type = "category")
            toLog <- toLog %>% rbind.data.frame(toLog_category)
        }

        upd_data <- old_names %in% colnames(obj)
        if (collapse::anyv(upd_data, TRUE)) {
            rlog::log_info("update_trait: Replacing names in data slot")
            obj$data <- obj$data %>%
                dplyr::rename_with(all_of(old_names[upd_data]),
                                   .fn = ~new_names[upd_data])
        }
        if (!is.null(obj$longitudinal)) {
            rlog::log_trace("update_trait: search to replace names in longitudinal")
            upd_long <- old_names %in% colnames(obj$longitudinal)
            if (collapse::anyv(upd_long, TRUE)) {
                rlog::log_info("update_trait: Replacing names in longitudinal")
                obj$longitudinal <- obj$longitudinal %>%
                    dplyr::rename_with(all_of(old_names[upd_long]),
                                       .fn = ~new_names[upd_long])
            }
        }
    }
    
    obj$traits <- new %>%
        collapse::ftransform(trait = new_trait)
    collapse::fselect(obj$traits, "new_trait") <- NULL

    
    toLog$expression <- testthat::capture_output(print(match.call()))
        
    obj$logs <- toLog

    obj <- .update_version(obj, ...)
    return(obj)
}


#' Update values of features or temporal features in a \code{an9elproject} object
#' 
#' If the data.frame to introduce have a column named \code{id_long}, it will be
#' assumed that the data to modify is purely temporal data, if not, then is supposed
#' to be purely normal features. The data.frame \code{values} always must include
#' a column called \code{id}.
#'
#' When updating not temporal features, it is possible to add new subjects. All the values
#' not given will be remain the same. If you want to remove the value you need to
#' explicitly pass NA
#'
#' When updating temporal features, it is not possible to add subjects.
#' It will make the project inconsistent. For the same reason, it is not
#' possible to add values to non existing traits.
#' @param obj object of class \emph{an9elproject}
#' @param values data.frame or similar with the data to incorporate to the project. See \code{details}
#' @param ... another parameters to pass to \code{.update_version}
#' @examples
#' obj <- an9elproject:::naiv
#' dt2 <- obj[] %>% dplyr::transmute(id = id, tr2 = tr2 * 2)
#' obj2 <- update_values(obj, dt2, new_version = c("0.1", "update_values data test"))
#' head(obj2$logs)
#' dt2_long <- obj$longitudinal %>% dplyr::transmute(id = id, id_long = id_long, incre = incre/2)
#' obj3 <- update_values(obj, dt2_long, new_version = c("0.1", "update_values long test"))
#' head(obj3$logs)
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
update_values <- function(obj, values, ...) {
    checkmate::assertMultiClass(obj, c("an9elproject", "list"))
    
    rlog::log_trace("update_values: check if is the last version online")
    checkmate::assertTRUE(an9elversions::check_last_version_online(obj))
    obj$logs <- NULL
    checkmate::assertDataFrame(values, col.names = "unique")
    checkmate::assertSubset(x = 'id', choices = colnames(values))
    
    has_long <- has_long_data(obj, values)
    if (has_long) {
        checkmate::assertSubset(as.character(values$id), choices = as.character(obj$data$id))
    }
    rlog::log_trace("update_values: check that all traits exists")
    checkmate::assertSubset(x = colnames(values), choices = obj$traits$trait)

    rlog::log_trace("update_values: check that all subjects exists")
    newSubjects <- !collapse::allv(values$id %in% obj$data$id, TRUE)
    if (newSubjects && has_long) {
        rlog::log_fatal("update_values: It is not posible to add NEW subjects in the longitudinal slot")
        cli::cli_alert_warning("Unknow subjects. Please add them before updating temporal data")
        return(invisible())
    } else if (newSubjects) {
        rlog::log_warn("update_values: some subjects will be added")
        cli::cli_alert_danger("New subjects will be added")
    }
    
    if (has_long) {
        rlog::log_debug("update_values: updating long")

        values <- tbl2plm(as.data.frame(values),
                          indexnames = unique(c("id", names(lindex(obj)))))
        
        new <- merge(long(obj), values)
        toLog <- toLogger(obj$longitudinal, new, type = "longitudinal")
        obj$longitudinal <- new 
    } else {
        rlog::log_debug("update_values: update non temporal features")
        checkmate::assertSubset(x = colnames(values), choices = colnames(obj))

        remain_values <- !(obj$data$id %in% values$id)
        if (any(remain_values)) {
            rlog::log_warn("update_values: values not passed preserved")
            cli::cli_alert_danger("Values not passed preserved")
            kk <- obj$data[remain_values, colnames(values)]
            values <- rbind(kk, values)
        }
        new <- obj$data %>%
            dplyr::select(-c(setdiff(colnames(values), "id"))) %>%
            dplyr::full_join(values, by = "id")

        toLog <- toLogger(obj[], new)
        obj$data <- new
    }
    
    if (nrow(toLog) > 0) {
        toLog$expression <- testthat::capture_output(print(match.call()))
    } else if (newSubjects) {
        toLog <- data.frame(time = date(), key = "new subjects",
                            variable = NA, old = NA, new = NA,
                            expression = testthat::capture_output(print(match.call())))
    }
    
    obj$logs <- toLog

    obj <- .update_version(obj, ...)
    return(obj)
}


#' Update or add miscelanea data for an \code{an9elproject} object
#'
#' Update or add miscelanea data for an \code{an9elproject} object
#' @param obj object of class \emph{an9elproject}
#' @param add_category "yes" to include more miscelanea categories
#' @examples
#' obj <- an9elproject:::naiv
#' obj <- miscelanea(obj)
#' @details To delete a miscelanea category insert "NA"
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
miscelanea <- function(obj, add_category = "no", update = TRUE) {
    rlog::log_trace("an9elproject: miscelanea")
    checkmate::assertClass(obj, "an9elproject")
    checkmate::assertFlag(update)
    checkmate::assertChoice(x = add_category, choices = c("yes", "no")) 

    if (is.null(obj$misc)) {
        misc <- list(
            project_affiliation = c(""),
            project_definition = c(""),
            project_inclusion_criteria = c(""),
            data_informed_consent = c(""),
            data_ethical_committee = c(""),
            data_access_committe = c(""),
            data_access_policy = c(""),
            data_acknowledgments = c(""),
            data_blood_collection = c(""),
            data_genotyping = c(""),
            data_genotyping_imputation = c(""),
            data_phenotyping = c(""),
            data_rnaseq = c("")
        )
        obj$misc <- misc
    }
    old_misc <- obj$misc
    
    cli::cli_h1("Please, insert or change miscelanea")
    cli::cli_alert_info("Insert the text between quotation marks, and do not use them in the text")
    
    for (dtinf in names(obj$misc)) {
        cli::cli_div(theme = list(span.emph = list(color = "red")))
        cli::cli_alert_info("Insert {.emph {dtinf}}:")
        cli::cli_alert_info("Actual value")
        cli::cli_alert_info("{obj$misc[[dtinf]]}")
        misc_new_def <- scan(what = "character",
             nmax = 1,
             sep = "#",
             na.strings = "NA",
             multi.line = FALSE,
             allowEscapes = TRUE)
        if (length(misc_new_def) == 1) {
            if (is.na(misc_new_def)) {
                obj$misc[[dtinf]] <- NULL
            } else {
                obj$misc[[dtinf]] <- misc_new_def
            }
        }
    }

    while (add_category == "yes") {
        cli::cli_h2("Adding more miscelanea data")
        cli::cli_inform("Insert the name of the new miscelanea")
        misc_name <- scan(what = "character", nmax = 1, multi.line = FALSE, allowEscapes = FALSE)
        cli::cli_inform("Insert the text for the previous miscelanea")
        misc_def <- scan(what = "character", nmax = 1, sep = "#", na.strings = "NA",
             multi.line = FALSE, allowEscapes = TRUE)
        obj$misc[[misc_name]] <- misc_def

        cli::cli_alert_info("Do you need to include more miscelanea data (yes/no)")
        add_category <- scan(what = "character", nmax = 1, multi.line = FALSE, allowEscapes = FALSE)
    }

    obj$logs <- toLogger(logger_list(old_misc, type = "miscelanea"),
                         logger_list(obj$misc, type = "miscelanea"), type = "miscelanea")
    
    if (update) {
        obj <- .update_version(obj)
    }
    return(obj)
}

#' Delete subjects in a \code{an9elproject} object
#'
#' Delete subjects observations in a \code{an9elproject} object
#' @param obj object of class \emph{an9elproject}
#' @param ids subjects ids to delete
#' @param ... optional parameter "new_version" of the \code{.update_version} function
#' @details All subject observations will be deleted, normal and temporal observations
#' @examples
#' obj <- an9elproject:::naiv
#' obj2 <- deleteTraits(obj, traits = c("tr.3", "incre"), new_version = c("0.9", "deleteTraits test"))
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
deleteSubjects <- function(obj, ids, ...) {
    checkmate::assertMultiClass(obj, c("an9elproject", "list"))
    rlog::log_trace("an9elproject: deleteSubjects")
    ## check if version is the latest one
    checkmate::assertTRUE(an9elversions::check_last_version_online(obj))
    checkmate::assertSubset(ids, choices = rownames(obj))


    new_data <- collapse::fsubset(obj$data, !(obj$data$id %in% ids))
    toLog <- toLogger(obj$data, new_data, type = "data")
    obj$data <- new_data
    
    if (!(is.null(obj$longitudinal))) {
        rlog::log_trace("an9elproject: deleteSubjects, deleting longitudinal observations")
        new_long <- collapse::fsubset(obj$longitudinal, !(obj$longitudinal$id %in% ids))
        toLog2 <- toLogger(obj$longitudinal, new_long, type = "longitudinal")
        toLog <- toLog %>% rbind.data.frame(toLog2)
        obj$longitudinal <- new_long
    }        

    obj$logs <- toLog
    obj <- .update_version(obj, ...)
    return(obj)
}


#' Delete longitudinal observation in a \code{an9elproject} object
#'
#' Delete longitudinal observations in a \code{an9elproject} object
#' @param obj object of class \emph{an9elproject}
#' @param toDelete data.frame with id,id_long columns and rows the observations to delete
#' @param ... optional parameter "new_version" of the \code{.update_version} function
#' @examples
#' obj <- an9elproject:::naiv
#' rem <- data.frame(id = c('id_1', 'id_1'), id_long = as.Date(c('2019-09-02', '2020-05-10')))
#' obj2 <- deleteObservations(obj, toDelete = rem)
#' 
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
deleteObservations <- function(obj, toDelete, ...) {
    checkmate::assertMultiClass(obj, c("an9elproject", "list"))
    checkmate::assertTRUE(an9elversions::check_last_version_online(obj))
    has_long <- has_long_data(obj, toDelete)
    if (!has_long) {
        cli::cli_alert_warning("Check parameters, is a an9elproject object? is toDelete a data.frame with id and id_long columns?")
        return(invisible())
    }
    checkmate::assertDataFrame(toDelete, ncols = 2)
    rlog::log_trace("an9elproject: deleteObservation: start deleting longitudinal observations")
    checkmate::assertSubset(toDelete$id, choices = rownames(obj))
    checkmate::assertTRUE(all(toDelete$id_long %in% obj$longitudinal$id_long))


    borrar <- rownames(tbl2plm(toDelete[, c("id", "id_long")], indexnames = c("id", "id_long")))
    new_long <- collapse::fsubset(long(obj), !(row.names(obj$longitudinal) %in% borrar))
    
    toLog <- toLogger(obj$longitudinal, new_long, type = "longitudinal")
    obj$longitudinal <- new_long
    
    obj$logs <- toLog
    obj <- .update_version(obj, ...)
    return(obj)
}

