% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/export_object.R
\name{an9el2plink}
\alias{an9el2plink}
\title{export data to plink}
\usage{
an9el2plink(
  obj,
  plink2 = TRUE,
  familyid,
  paternalid,
  maternalid,
  sexid,
  phenotypeid,
  covariatesid
)
}
\arguments{
\item{obj}{object of class \emph{an9elproject}}

\item{plink2}{export to plink1 or plink2}

\item{familyid}{the name of the variable family}

\item{paternalid}{the name of the variable Father}

\item{maternalid}{The name of the variable Mother}

\item{sexid}{The name of the variable Sex}

\item{phenotypeid}{Optional name of the variable affected}

\item{covariatesid}{Optional name of the covariates}
}
\description{
Creates the files \code{pedFile} and the \code{covariateFile} to use plink.
}
\details{
For character version of sex and affected use plink2 = TRUE, the default.
For numeric versions use plink2 = FALSE
}
\examples{
\dontrun{
retr <- get_project('RETROVE')
an9el2plink(retr, sexid = "sex", phenotypeid = "fc_vc")
}
}
\author{
person("Angel", "Martinez-Perez",
email = "angelmartinez@protonmail.com",
role = c("aut", "cre"),
comment = c(ORCID = "0000-0002-5934-1454"))
}
