
test_that("an9el2plink function tests", {
    data("naiv")
    an9el2plink(naiv, plink2 = TRUE, sexid = "sex", covariatesid = c("tr1", "tr2"))
    
    expect_true(checkmate::testFileExists("naiv.ped"))
    expect_true(checkmate::testFileExists("naiv.cov"))
    unlink("naiv.ped")
    unlink("naiv.cov")
    
    
    an9el2plink(naiv,plink2 = FALSE, sexid = "sex", familyid= "tr1", paternalid= "tr2",maternalid="tr.3", phenotypeid = "tr+5")
    expect_true(checkmate::testFileExists("naiv.ped"))
    expect_false(checkmate::testFileExists("naiv.cov"))
    unlink("naiv.ped")
    an9el2plink(naiv)
    expect_true(checkmate::testFileExists("naiv.ped"))
    expect_false(checkmate::testFileExists("naiv.cov"))
    unlink("naiv.ped")

})
