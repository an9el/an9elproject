test_that("use object", {
    data("naiv")
    des <- describe(naiv)
    des2 <- describe(naiv, colnames(naiv)[2:5])
    checkmate::assertTibble(des, nrows = 7, ncols = 13)
    checkmate::assertTibble(des2, nrows = 4, ncols = 13)

    checkmate::assertTibble(head(naiv), nrow = 6, ncols = 7)
    checkmate::assertDataFrame(head(long(naiv)), nrows = 6, ncols = 5)
    checkmate::assertNull(findDuplicates("character"))
})
